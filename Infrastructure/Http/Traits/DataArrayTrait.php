<?php
namespace Infrastructure\Http\Traits;



trait DataArrayTrait {
    public function pageTypeArray(){
        return ['Event', 'News','About_Us','Notice','Admission','Academic'];
    }

    public function academicsArray(){
        return['Upper','Middle','Lower','PreSchool'];
    }
    public function positionArray(){
        return['Head Master','Deputy Head','Assistant Head'];
    }

}
