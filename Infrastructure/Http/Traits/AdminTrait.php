<?php
namespace Infrastructure\Http\Traits;


use App\Admin\Message\Repositories\MessageRepository;
use App\Admin\Page\Models\Page;
use App\Admin\Setting\Repositories\SettingRepository;

trait AdminTrait
{
    protected $data;
    private $setting;
    private $customerMessage;

    public function __construct(SettingRepository $setting, MessageRepository $message)
    {
//        load repositories
        $this->setting = $setting;
        $this->message = $message;
        $this->data['messages'] = $this->getCustomerMessages();
        $this->data['setting'] = $this->getSetting();
    }

    public function getSetting()
    {
        return $this->setting->getRow();
    }

    public function getCustomerMessages()
    {

        return $this->customerMessage->getResults('*', ['status' => 'unseen']);
    }




}
