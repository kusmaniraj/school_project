<?php
namespace Infrastructure\Http\Traits;

use App\Message;
use App\PageCategory;
use App\Setting;

trait AlertMessage {
   public function getAlertMessage($statusType,$actionType,$info){
       if($statusType=='success'){
           return ['success'=>$actionType . ' a '.$info];
       }else{
           return ['error'=>'Could not '.$actionType . ' a '.$info];
       }
   }
}
