<?php
namespace Infrastructure\Http\Traits;
trait HelperTrait {


    public  function getEnumValues($table, $column)
    {
        $type = \DB::select( \DB::raw("SHOW COLUMNS FROM $table WHERE Field = '$column'") )[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach( explode(',', $matches[1]) as $key=>$value )
        {
            $v = trim( $value, "'" );
            array_push($enum, $v);
        }
        return $enum;
    }
    public function trimTitle($text){
        return str_replace('_',' ',$text);
    }
    public function concatTitle($text){
        return str_replace(' ','_',$text);
    }
    public function replaceWhiteSpace($text){
        return str_replace('%20','-',$text);
    }
    public function stringReplace($with,$from,$string){
        return str_replace($with,$from,$string);
    }

}
