<?php

namespace Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;
use Config;
use DB;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if (\Schema::hasTable('menus')) {
            $menuPages = $this->navMenus();
            if ($menuPages) {
                foreach ($menuPages as $i => $page) {

                    $prevParentInfo= DB::table('menus')->where(['title'=>$page['title'],'parent_id'=>0])->first();
                    if($prevParentInfo || $prevParentInfo !=null){
                        $parentId=$prevParentInfo->id;

                    }else{
                        $storeData = [
                            'title' => $page['title'],
                            'url' => $page['url'],
                            'parent_id' => 0,
                            'position' => $i
                        ];
                        $parentId = DB::table('menus')->insertGetId($storeData);
                    }


                    if ($parentId  && isset($page['children']) && $page['children']) {
                        foreach ($page['children'] as $childKey => $childPage) {
                            $prevChildInfo= DB::table('menus')->where(['title'=>$childPage['title'],'parent_id'=>$parentId])->first();
                            if($prevChildInfo==null){
                                $storeChildData = [
                                    'title' => $childPage['title'],
                                    'url' => $childPage['url'],
                                    'parent_id' =>$parentId,
                                    'position' => $childKey
                                ];
                                DB::table('menus')->insert($storeChildData);
                            }


                        }

                    }

                }
            }
        }
    }

    private function navMenus()
    {
        return [
            [
                'title' => 'Home',
                'url' => url('home'),
                'children' => []
            ],
            [
                'title' => 'About Us',
                'url' => '',
                'children' => []
            ],

            [
                'title' => 'Contact',
                'url' => url('/contact'),
                'children' => []
            ],
            [
                'title' => 'Gallery',
                'url' => url('/gallery'),
                'children' => []
            ],
            [
                'title' => 'Admission',
                'url' => url('admission'),
                'children' => []
            ],
            [
                'title' => 'Academic',
                'url' => url('/academic'),
                'children' => $this->getChildrenList('Academic', url('academic/'))
            ],

        ];
    }

    private function getChildrenList($page, $url)
    {
        $mappedPageMenu = [];
        if (\Schema::hasTable('pages')) {
            $pageResult =DB::table('pages')->where(['status' => 'active', 'page_type' => $page])->get();

            foreach ($pageResult as $i => $data) {
                array_push($mappedPageMenu, ['title' => $data->title, 'url' => $url . '/' . str_replace(' ', '-', $data->title)]);
            }
        }

        return $mappedPageMenu;
    }
}
