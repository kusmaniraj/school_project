<?php

namespace Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;
use Config;
use DB;

class SettingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if (\Schema::hasTable('settings')) {
            $setting = DB::table('settings')->first();
            if ($setting) //checking if table is not empty
            {
//                $config = array(
//                    'company_name' => $setting->company_name,
//                    'company_short_name' => $setting->company_short_name,
//                    'logo_img' => $setting->logo_img,
//                    'company_slogan' => $setting->company_slogan,
//                    'address' => $setting->address,
//                    'email' => $setting->email,
//                    'phone_number' => $setting->phone_number,
//                    'fax_number' => $setting->fax_number,
//                    'company_version' => $setting->company_version,
//                    'company_start_date' => $setting->company_start_date
//
//                );

                Config::set('setting', (array)$setting);
            }
        }
    }
}
