<?php

namespace Infrastructure\Providers;


use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use DB;
use Config;

class MailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $email='admin@admin.com';
        $companyName=env('APP_NAME');
//get address from table
        if (\Schema::hasTable('settings')) {
            $setting = DB::table('settings')->first();
            if ($setting) //checking if table is not empty
            {
               $email=$setting->email;
                $companyName=$setting->company_name;
            }
        }


        if(Schema::hasTable('mail_settings')){
            $mail_setting=DB::table('mail_settings')->first();
            if($mail_setting){
                $configData=[
                    'driver'=>$mail_setting->driver,
                    'host'=>$mail_setting->host,
                    'port'=>$mail_setting->port,
                    'from' => ['address' => $email, 'name' =>$companyName ],

                    'username'=>$mail_setting->username,
                    'encryption'=>$mail_setting->encryption,
                    'password'=>$mail_setting->password,
                    'pretend'=> false,

                ];
                Config::set('mail',$configData);
            }


        }
    }
}
