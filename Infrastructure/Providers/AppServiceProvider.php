<?php

namespace Infrastructure\Providers;

use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
//use League\Flysystem\Config;
use DB;
use Config;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       Schema::defaultStringLength(191);


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (Schema::hasTable('settings')) {
            $setting = DB::table('settings')->first();
            if ($setting) //checking if table is not empty
            {
                Config::set('app.name', $setting->company_name);
            }
        }
    }
}
