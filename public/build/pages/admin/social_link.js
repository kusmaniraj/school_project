var social_link_module = new Social_link();
function Social_link() {
    var $self = this;
    var $pageName = $('#page_social_link');

    var $tableName = $('#social_link_table');
    var $modalName = $('#social_link_modal_form');
    var $formName = $('#social_link_form');
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var dataTables = "";
     this.staffId="";

    //Import from shared js
    var sharedModule = new Shared();




    this.bind = function () {
        //add content
        $pageName.on('click', '#add_social_link_btn', function () {
            sharedModule.emptyForm($formName);
            $formName.find('input[name="staffId"]').val($self.staffId);
            sharedModule.removeValidationErrors($formName);
            $modalName.modal('show');
            $modalName.find('.modal-title').text('Add Page Social_link form');
            $formName.find('button[type="submit"]').text('Created');
        })


        //submit
        $formName.on('submit', function (e) {
            e.preventDefault();
            sharedModule.removeValidationErrors($formName);
            var id = $(this).find('input[name="id"]').val();
            var formInput = $(this).serialize() + '&_token=' + csrf_token;

            if (id) {
                $self.update(formInput, id);
            } else {
                $self.store(formInput);
            }


        })



        //edit
        $tableName.on('click', '.edit_social_link_btn', function () {
            sharedModule.removeValidationErrors($formName);
            var id=$(this).data('id');
            $modalName.modal('show');
            $self.edit(id);


        });

        //delete
        $tableName.on('click', '.delete_social_link_btn', function () {
            var id=$(this).data('id');
            if(confirm('Are You sure want to remove ?')==true){
                $self.delete(id)
            }else{
                return false;
            }


        });


        //    view Myteam page
        //
        $pageName.on('click','#back_to_my_team_btn',function(){

            //call function of staff.js method
            staff_module.showStaffPage($pageName);
        })
    }

    this.getSocial_links = function (staffId) {

        $.get(base_url + '/admin/staff/social_link/getSocialLinks/'+staffId, function (resp) {
            var contentListHtml = "";
            $.each(resp, function (i, v) {
                contentListHtml += '<tr>' +
                    '<td>' + (i + 1) + '</td>' +

                    '<td>' + v.name + '</td>' +
                    '<td>' + v.url + '</td>' +


                    '<td>' +
                    '<a href="#" data-id="'+ v.id+'" class=" edit_social_link_btn btn btn-primary"><i class="fa fa-edit"></i></a> &nbsp;' +
                    '<a href="#"  data-id="'+ v.id+'" class=" delete_social_link_btn btn btn-danger"><i class="fa fa-trash"></i></a> ' +
                    '</td>' +

                    '</tr>';
            });
            if (dataTables != "") {
                dataTables.destroy();
            }
            $pageName.find('tbody').html(contentListHtml);
            //se5t data tables
            dataTables = $tableName.DataTable();



        })


    }
    this.store = function (formData) {

        $.post(base_url + '/admin/staff/social_link/store', formData, function (resp) {
            if (resp.validation_errors) {
                sharedModule.validation_errors($formName,resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($pageName,'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName,'success', resp.success);

                sharedModule.emptyForm($formName);

                $self.getSocial_links( $self.staffId);
                $modalName.modal('hide');
            }

        })
    }
    this.update = function (formInput, id) {
        var formData = formInput;
        $.post(base_url + '/admin/staff/social_link/update/' + id, formData, function (resp) {
            if (resp.validation_errors) {
                sharedModule.validation_errors($formName,resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($pageName,'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName,'success', resp.success)

                $self.getSocial_links( $self.staffId);
                $modalName.modal('hide');
            }
        })
    }
    this.edit = function (id) {
        $.get(base_url + '/admin/staff/social_link/' + id + '/edit', function (resp) {
            var data = resp;
            $formName.find('input[name="id"]').val(data.id);
            $formName.find('input[name="staffId"]').val(data.staffId);
            $formName.find('input[name="name"]').val(data.name);
            $formName.find('input[name="url"]').val(data.url);






            $formName.find('input[name="status"][value="' + data.status + '"]').prop("checked", true);
            $formName.find('button[type="submit"]').text('Update');
            $modalName.find('.modal-title').text('Edit Page Content form');

        })
    }
    this.delete = function (id) {

        $.get(base_url + '/admin/staff/social_link/delete/' + id,function (resp) {
            if (resp.error) {
                sharedModule.alertMessage($pageName,'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName,'success', resp.success)

                $self.getSocial_links( $self.staffId);
            }
        })
    }


    this.showSocialLinksPage=function(selectorPage,staffId,name){
        selectorPage.hide('slide',{direction:"left"},500,function(){
            $pageName.find('#back_to_my_team_btn').attr('data-id',staffId);

            $pageName.find('.box .box-title strong').after('&nbsp;<span>'+name+'</span>');
            $pageName.show();
            $self.staffId=staffId;
            $self.getSocial_links(staffId);
        });


    }


    //initilaize  function
    this.init=function(){



        $self.bind();
    }();
}



