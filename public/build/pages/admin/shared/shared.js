
function Shared() {
    //selector format is in $('#form')
    var $self = this;

    //param 1=selector of form  eg()selector=>$('#form')
    this.emptyForm = function (selector) {
        selector[0].reset();
        selector.find('input[type="hidden"]').val("");
        selector.find('textarea').text("");
        selector.find('input[name="status"][value="active"]').prop("checked", true);
        selector.find('input#thumbnail').val('');
        selector.find('img#holder').attr('src', '');
        selector.find('select').val(0);


        //optional
        //CKEDITOR.instances.description.setData('');

        selector.find('.removeImgBtn').addClass('hidden');
}

    // param 1=formName param2=message in array
    this.validation_errors = function (selector,$messages) {
        //console.log(selector)
        var errorHtml = '<div class=" validation-errors alert alert-danger"> <ul>';
        $.each($messages, function (i, v) {
            errorHtml += '<li>' + v + '</li>'
        })
        errorHtml += '</ul></div>';
        selector.prepend(errorHtml);
    }

    //parram 1= selector , pararam2=msgType(error or success) and param3=message of error
    this.alertMessage = function (selector,msgType, msg) {
        var errorHtml = '';

        if (msgType == 'error') {
            alertify.success('Error '+msg);
            errorHtml = '<div class=" alert-msg alert alert-danger"><strong>Error !! </strong>' + msg + '</div>'
        }
        if (msgType == 'success') {
            alertify.success('Success to '+msg);
            errorHtml = '<div class=" alert-msg alert alert-success"> <strong>Successfully !! </strong>' + msg + '</div>'
        }
        selector.prepend(errorHtml);
        window.setTimeout(function () {
            selector.find('.alert-msg').slideUp(500, function () {
                $(this).remove();
            });
        }, 3000)
    }

    //param=selector
    this.removeValidationErrors = function (selector) {


        selector.find('.validation-errors').remove();


    this.init=function(){}
    }


}





