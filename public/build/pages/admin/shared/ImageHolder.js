function ImageHolder(inputSel,holderSel,removeBtnSel) {
    var $self = this;
    var $holderSel = $(holderSel),
        $removeHolderBtnSel = $(removeBtnSel),
        $imageField = $(inputSel);

    this.bind = function () {
        //show and hide holder image
        $imageField.on('change', function () {
           $self.hideShowImage();
        });

        //remove holder image and empty image field
        $removeHolderBtnSel.on('click', function () {
            $holderSel.attr('src', '');
            $imageField.val('');
            $(this).hide();
        })
    }


    this.hideShowImage=function(){
        if ($imageField.val() != '') {
            $removeHolderBtnSel.show();
        } else {
            $removeHolderBtnSel.hide();
        }
    }

    this.init = function () {
        $self.bind();
        $self.hideShowImage();
    }();


}





