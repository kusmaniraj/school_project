var menu_module = new Menu();
function Menu() {
    var $self = this;
    var $pageName = $('#page_menu');
    var $loader = $('.loader-wrapper');


    var $formName = $('#menu_form');
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var js_tree = "";
    var pageMenuList = [];


    //Import from shared js
    var sharedModule = new Shared();

    this.bind = function () {
        //add content
        $pageName.on('click', '#add_menu_btn', function () {
            sharedModule.emptyForm($formName);
            sharedModule.removeValidationErrors($formName);
            $pageName.find('.box-title').html(' <strong>Add Page Menu form</strong>');
            $formName.find('button[type="submit"]').text('Created');
        })


        //submit
        $formName.on('submit', function (e) {
            e.preventDefault();
            sharedModule.removeValidationErrors($formName);
            var id = $(this).find('input[name="id"]').val();
            var formInput = $(this).serialize() + '&_token=' + csrf_token;

            if (id) {
                $self.update(formInput, id);
            } else {
                $self.store(formInput);
            }


        })


        //save tree structure
        $('#saveTreeBtn').on('click', function () {

            $self.saveTreeChanges();
        })
        //refresh tree
        $('#refreshTreeBtn').on('click', function () {

            $self.initializeTree();
        });

        //check image available or not
        $formName.find('input[name="featured_img"]').change(function () {
            $formName.find('.removeImgBtn').removeClass('hidden');
        });
        $formName.on('click', '.removeImgBtn', function () {
            $formName.find('input#thumbnail').val('');
            $formName.find('img#holder').attr('src', '');
            $(this).addClass('hidden');


        })

        $formName.on('change', 'select[name="title"]', function () {
            var title = $(this).val();
            var url = $(this).find('option:selected').attr('data-url');
            var childCount = $(this).find('option:selected').attr('data-children');
            if (url) {
                $self.setValPageUrl('input[name="url"]',url);
               if(parseInt(childCount) >0){
                   $self.setChildrenTitleUrl(title)
                   $('#childBlock').show();
               }

            }
        })
        $formName.on('change', 'select[name="childTitle"]', function () {
            var url = $(this).find('option:selected').attr('data-url');

            if (url) {
                $self.setValPageUrl('input[name="childUrl"]',url);


            }
        })
    }
    this.initializeTree = function () {
        $('#jstree').jstree('destroy');
        $.get(base_url + '/admin/menu/getMenus', function (resp) {
            //console.log(resp);
            js_tree = $("#jstree").jstree({
                "animation" : 0,
                "core": {
                    // so that create works
                    "check_callback": function(operation, node, node_parent){
                        console.log(node);
                        if (operation === 'move_node' && node.parent !== node_parent.id) {
                            return false;
                        }else{
                            return true;
                        }

                    },
                    'data': resp,
                    "checkbox" : {
                        "keep_selected_style" : false,

                    },

                },
                "plugins": ["contextmenu",'checkbox',"dnd"],
                "contextmenu": {
                    "items": function ($node) {
                        //var tree = $("#tree").jstree(true);
                        return {

                            "Edit": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Edit",
                                "action": function (obj) {
                                    $self.edit($node.id);
                                }
                            },

                        };
                    }
                }
            })
            //if (js_tree != "") {
            //    $('#jstree').jstree(true).settings.core.data = resp;
            //    //$('#jstree').jstree(true).refresh();
            //
            //}


        })


    }


    this.store = function (formData) {

        $.post(base_url + '/admin/menu/store', formData, function (resp) {
            if (resp.validation_errors) {
                sharedModule.validation_errors($formName, resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($pageName, 'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName, 'success', resp.success);

                sharedModule.emptyForm($formName);

                $self.initializeTree();

            }

        })
    }
    this.update = function (formInput, id) {
        var formData = formInput;
        $.post(base_url + '/admin/menu/update/' + id, formData, function (resp) {
            if (resp.validation_errors) {
                sharedModule.validation_errors($formName, resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($pageName, 'error', resp.error)
            } else {
                sharedModule.alertMessage($pageName, 'success', resp.success)

                $self.initializeTree();
            }
        })
    }
    this.edit = function (id) {
        //console.log(id)
        $.get(base_url + '/admin/menu/' + id + '/edit', function (resp) {
            var data = resp;
            $formName.find('input[name="id"]').val(data.id);

            $formName.find('input[name="title"]').val(data.title);
            $formName.find('input[name="url"]').val(data.url);


            //image
            $formName.find('input#thumbnail').val(data.featured_img);
            $formName.find('img#holder').attr('src', data.featured_img);
            if ($formName.find('input[name="featured_img"]').val() == "") {
                $formName.find('.removeImgBtn').addClass('hidden');
            } else {
                $formName.find('.removeImgBtn').removeClass('hidden');
            }

            //

            $formName.find('input[name="status"][value="' + data.status + '"]').prop("checked", true);
            $formName.find('button[type="submit"]').text('Update');
            $pageName.find('.box-title').html('<strong>Edit Page Menu form</strong>');

        })
    }

    this.saveTreeChanges = function () {
        var treeData = $('#jstree').jstree(true).get_json();
        console.log(treeData)
        var data = '_token=' + csrf_token;
        $.each(treeData, function (i, parent) {
            data += '&id[]=' + parent.id;
            data += '&selected[]=' + parent.state.selected;

            $.each(parent.children, function (k, child) {
                data += '&children[' + parent.id + '][id][]=' + child.id;
                data += '&children[' + parent.id + '][selected][]=' + child.state.selected;
            })

        })

        $.post(base_url + '/admin/menu/saveTree/', data, function (resp) {
            //console.log(resp);
            $self.initializeTree();
        })

    }

    this.setChildrenTitleUrl = function (title) {
        pageMenuList.forEach(function (v) {
            //console.log( v.title+title);
            if (v.title === title) {
                var optionHtml = '<option selected disabled>Select Child Menu Title</option>';

                v.children.forEach(function (val) {
                    optionHtml += '<option value="' + val.title + '" data-url="' + val.url + '" >' + val.title + '</option>';
                });
                $self.printPageMenuList('select[name="childTitle"]',optionHtml);


            }
        })
    }
    this.printPageMenuList = function (selector,optionHtml) {

        $formName.find(selector).html(optionHtml);
    }
    this.setValPageUrl = function (selector,url) {

        $formName.find(selector).val(url);
    }

    //initilaize  function
    this.init = function () {


        $self.initializeTree();

        $self.bind();
        $('#childBlock').hide();
    }();
}



