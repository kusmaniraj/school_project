var page_categorymodule=new PageCategory();
function  PageCategory(){
    var $self=this;
    var $pageName=$('#page_category');
    var selector = '.context-menu-category';
    var $formName = $('#page_category_form');
    var csrf_token=$('meta[name="csrf-token"]').attr('content');
    var sharedModule=new Shared();

    //intialize the list-categories
    this.init=function(){

    //    get Categories;
        $self.getPageCategories();
    }

    this.bind=function(){
        $formName.on('submit',function(e){
            e.preventDefault();
            sharedModule.removeValidationErrors($formName);
            var id=$(this).find('input[name="id"]').val();
            var formInput=$(this).serialize()+'&_token='+csrf_token;

            if(id){
                $self.update(formInput,id);
            }else{
                $self.store(formInput);
            }


        })

        $pageName.on('click','#addCategoryBtn',function(){
            sharedModule.emptyForm($formName);
            sharedModule.removeValidationErrors($formName);
            $pageName.find('.box-title').html(' <strong>Add Page Category Form</strong>');
            $formName.find('button[type="submit"]').text('Created');
        })


        //check image available or not
        //check image available or not
        $formName.find('input[name="featured_img"]').change(function () {
            $formName.find('.removeImgBtn').removeClass('hidden');
        });
        $formName.on('click', '.removeImgBtn', function () {
            $formName.find('input#thumbnail').val('');
            $formName.find('img#holder').attr('src', '');
            $(this).addClass('hidden');


        })
    }

    this.getPageCategories=function(){
        $.get(base_url+'/admin/getPageCategories',function(resp){
           var categoryListHtml="";
            $.each(resp,function(i,v){
                categoryListHtml +='<a href="#" id="'+ v.id+'" class="list-group-item ist-group-item-action context-menu-category"><i class="fa fa-folder" style="color:#3c8dbc; margin-right: 15px"></i>'+ v.title+'</a>';
            });
            $pageName.find('#category_list').html(categoryListHtml);

            //set context menu
            $(selector).contextMenu('menu-1', {

                bindings: {

                    'edit': function (selector) {
                        var id = selector.id;
                        $self.edit(id);



                    },
                    'delete': function (selector) {
                        $self.emptyForm();
                        var id = selector.id;
                        if(confirm('Are you sure want to remove ?')==true){
                            $self.delete(id);
                        }else{
                            return false;
                        }


                    },
                    'contents': function (selector) {

                        var contentName=$('a#'+selector.id).text();
                        $self.contents(contentName)

                    }

                }

            });

        })

    }
    this.store=function(formData){

        $.post(base_url+'/admin/page_category',formData,function(resp){
            if(resp.validation_errors){
                sharedModule.validation_errors($formName, resp.validation_errors);
            }else if(resp.error){
                sharedModule.alertMessage($pageName, 'error', resp.error)
            }else{
                sharedModule.alertMessage($pageName, 'success', resp.success)



                sharedModule.emptyForm($formName);

                $self.getPageCategories();
            }

        })
    }
    this.update=function(formInput,id){
        var formData=formInput+'&_method=put';
        $.post(base_url+'/admin/page_category/'+id,formData,function(resp){
            if(resp.validation_errors){
                sharedModule.validation_errors($formName, resp.validation_errors);
            }else if(resp.error){
                sharedModule.alertMessage($pageName, 'error', resp.error)
            }else{
                sharedModule.alertMessage($pageName, 'success', resp.success)





                $self.getPageCategories();
            }
        })
    }
    this.edit=function(id){
        $.get(base_url + '/admin/page_category/' + id + '/edit', function (resp) {
            var data=resp;
          $formName.find('input[name="id"]').val(data.id);
            $formName.find('input[name="title"]').val(data.title);



            //image
            $formName.find('input#thumbnail').val(data.featured_img);
            $formName.find('img#holder').attr('src', data.featured_img);
            if ($formName.find('input[name="featured_img"]').val() == "" ) {
                $formName.find('.removeImgBtn').addClass('hidden');
            } else {
                $formName.find('.removeImgBtn').removeClass('hidden');
            }


            $formName.find('input[name="status"][value="'+data.status+'"]').prop("checked", true);
            $formName.find('button[type="submit"]').text('Update');
            $pageName.find('.box-header .box-title strong').text('Edit Page Category form');
        })
    }
    this.contents=function(pageContentName){
        window.location.href=base_url+'/admin/page_content/#'+ pageContentName.split(' ').join('_');

    }

    this.delete=function(id){
        var formData=$(this).serialize()+'&_token='+csrf_token+'&_method=delete';
        $.post(base_url + '/admin/page_category/' + id , formData,function (resp) {
            if(resp.error){
                sharedModule.alertMessage($pageName, 'error', resp.error)
            }else{
                sharedModule.alertMessage($pageName, 'success', resp.success)

                $self.getPageCategories();
            }
        })
    }


    $(function(){
        $self.init();
        $self.bind();
    })
}



