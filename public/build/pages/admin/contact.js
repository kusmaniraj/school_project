var contact_module = new Contact();
function Contact() {
    var $self = this;
    var $contactSection = $('#contact');
    var $tableName = $('#contact_table');
    var $modalName = $('#contact_modal_form');
    var $formName = $('#contact_form');
    var contactName = $tableName.attr('data-contact');
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var dataTables = "";


    //Import from shared js
    var sharedModule = new Shared();


    this.bind = function () {
        //add content
        $contactSection.on('click', '#add_contact_btn', function () {
            sharedModule.emptyForm($formName);

            sharedModule.removeValidationErrors($formName);
            $modalName.modal('show');
            $modalName.find('.modal-title').text('Add ' + contactName + ' Form');
            $formName.find('button[type="submit"]').text('Created');


        })


        //submit
        $formName.on('submit', function (e) {
            e.preventDefault();
            sharedModule.removeValidationErrors($formName);
            var id = $(this).find('input[name="id"]').val();
            var formInput = $(this).serialize() + '&_token=' + csrf_token;

            if (id) {
                $self.update(formInput, id);
            } else {
                $self.store(formInput);
            }


        })


        //edit
        $tableName.on('click', '.edit_contact_btn', function () {
            sharedModule.removeValidationErrors($formName);
            var id = $(this).data('id');
            $modalName.modal('show');
            $self.edit(id);


        });

        //delete
        $tableName.on('click', '.delete_contact_btn', function () {
            var id = $(this).data('id');

            alertify.confirm("Are You sure want to remove.",
                function(){
                    $self.delete(id)

                },
                function(){
                    //alertify.error('Cancel');
                    alertify.confirm().close();
                    return false;
                }).setHeader('Remove Enquiry Box');


        });


    }

    this.getContacts = function () {


        $.get(base_url + '/admin/contact/getContacts/' + contactName, function (resp) {
            var contentListHtml = "";
            $.each(resp, function (i, v) {

                contentListHtml += '<tr>' +
                    '<td>' + (i + 1) + '</td>' +
                    '<td>' + v.title + '</td>';
                if (v.status == 'active') {
                    contentListHtml += '<td> <label  class="label label-success">' + v.status + '</label> </td>';
                } else {
                    contentListHtml += '<td> <label  class="label label-danger">' + v.status + '</label> </td>';
                }


                contentListHtml += '<td>' +
                    '<a href="#" data-id="' + v.id + '" class=" edit_contact_btn btn btn-primary"><i class="fa fa-edit"></i></a> &nbsp;' +
                    '<a href="#"  data-id="' + v.id + '" class=" delete_contact_btn btn btn-danger"><i class="fa fa-trash"></i></a> ' +
                    '</td>' +

                    '</tr>';
            });
            if (dataTables != "") {
                dataTables.destroy();
            }
            $contactSection.find('tbody').html(contentListHtml);

            //set data tables
            dataTables = $tableName.DataTable();




        })


    }
    this.store = function (formData) {
        formData += '&contactType=' + contactName;
        $.post(base_url + '/admin/contact/store/', formData, function (resp) {
            if (resp.validation_errors) {
                sharedModule.validation_errors($formName, resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($contactSection, 'error', resp.error)
            } else {
                sharedModule.alertMessage($contactSection, 'success', resp.success);

                sharedModule.emptyForm($formName);

                $self.getContacts();
                $modalName.modal('hide');
            }

        })
    }
    this.update = function (formInput, id) {
        var formData = formInput + '&contactType=' + contactName;
        $.post(base_url + '/admin/contact/update/' + id, formData, function (resp) {
            if (resp.validation_errors) {
                sharedModule.validation_errors($formName, resp.validation_errors);
            } else if (resp.error) {
                sharedModule.alertMessage($contactSection, 'error', resp.error)
            } else {
                sharedModule.alertMessage($contactSection, 'success', resp.success)

                $self.getContacts();
                $modalName.modal('hide');
            }
        })
    }
    this.edit = function (id) {
        $.get(base_url + '/admin/contact/' + id + '/edit', function (resp) {
            var data = resp;
            $.each(data, function (dbField, dbValue) {
                $formName.find('input,select').each(function (i, v) {
                    var name = $(this).attr('name');
                    var type=$(this).attr('type');

                    if (name == dbField) {
                        if(type=='radio'){
                            if( $(this).val()==dbValue){
                                $(this).prop("checked", true);
                            }

                        }else{
                            $(this).val(dbValue);
                        }

                    }

                })
            })


            // $formName.find('input[name="status"][value="' + data.status + '"]').prop("checked", true);

            $formName.find('button[type="submit"]').text('Update');
            $modalName.find('.modal-title').text('Edit ' + contactName + ' Form');

        })
    }
    this.delete = function (id) {

        $.get(base_url + '/admin/contact/delete/' + id, function (resp) {
            if (resp.error) {
                sharedModule.alertMessage($contactSection, 'error', resp.error)
            } else {
                sharedModule.alertMessage($contactSection, 'success', resp.success)

                $self.getContacts();
            }
        })
    }


    //initilaize  function
    this.init = function () {


        $self.getContacts();
        $self.bind();
    }();
}



