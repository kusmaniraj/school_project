var staff_module = new Staff();
function Staff() {
    var $self = this;
    var $pageName = $('#page_staff');
    var $tableName = $('#staff_table');
    var $modalName = $('#staff_modal_form');
    var $formName = $('#staff_form');
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var dataTables = "";
    var ajax = "";

    //Import from shared js
    var sharedModule = new Shared();


    this.bind = function () {
        //add content
        $pageName.on('click', '#add_staff_btn', function () {
            sharedModule.emptyForm($formName);
            sharedModule.removeValidationErrors($formName);
            $modalName.modal('show');
            $modalName.find('.modal-title').text('Add My Team Page Form');
            $formName.find('button[type="submit"]').text('Created');
        })


        //submit
        $formName.on('submit', function (e) {
            e.preventDefault();
            sharedModule.removeValidationErrors($formName);
            var id = $(this).find('input[name="id"]').val();
            var formInput = $(this).serialize() + '&_token=' + csrf_token;

            if (id) {
                $self.update(formInput, id);
            } else {
                $self.store(formInput);
            }


        })


        //edit
        $tableName.on('click', '.edit_staff_btn', function () {
            sharedModule.removeValidationErrors($formName);
            var id = $(this).data('id');
            $modalName.modal('show');
            $self.edit(id);


        });

        //delete
        $tableName.on('click', '.delete_staff_btn', function () {
            var id = $(this).data('id');
            if (confirm('Are You sure want to remove ?') == true) {
                $self.delete(id)
            } else {
                return false;
            }


        });

        //check image available or not
        $formName.find('input[name="profile_img"]').change(function () {
            $formName.find('.removeImgBtn').removeClass('hidden');
        });
        $formName.on('click', '.removeImgBtn', function () {
            $formName.find('input#thumbnail').val('');
            $formName.find('img#holder').attr('src', '');
            $(this).addClass('hidden');


        })


        //    view social links page
        $tableName.on('click', '.viewSocialLinks', function () {
            var id = $(this).data('id');
            var name = $(this).data('name');
            //call function of social_lionk.js method
            social_link_module.showSocialLinksPage($pageName, id, name);

        })
    }

    this.getStaffs = function () {

        $.get(base_url + '/admin/staff/getStaffs').done(success).catch((err)=>console.log('Error to get my team list'));
        //success
        function success(data) {
            var contentListHtml = "";
            $.each(data, function (i, v) {
                var information_limit = v.information.split(' ', 30).join(' ')
                contentListHtml += '<tr>' +
                    '<td>' + (i + 1) + '</td>' +

                    '<td>' + v.name + '</td>' +
                    '<td>' + v.position + '</td>' +
                    '<td>' + information_limit + '...</td>' +
                    '<td><a href="#" data-id="' + v.id + '" data-name="' + v.name + '" class="viewSocialLinks">Social Links &nbsp; <label class="label label-primary">' + v.social_links.length + '</label></a></td>' +

                    '<td>' +
                    '<a href="#" data-id="' + v.id + '" class=" edit_staff_btn btn btn-primary"><i class="fa fa-edit"></i></a> &nbsp;' +
                    '<a href="#"  data-id="' + v.id + '" class=" delete_staff_btn btn btn-danger"><i class="fa fa-trash"></i></a> ' +
                    '</td>' +

                    '</tr>';
            });
            if (dataTables != "") {
                dataTables.destroy();
            }
            $pageName.find('tbody').html(contentListHtml);
            //se5t data tables
            dataTables = $tableName.DataTable();
        }


    }

    this.store = function (formData) {

        ajax = $.post(base_url + '/admin/staff/store', formData).done(success).fail(error);

        //success store
        function success(resp) {

            sharedModule.alertMessage($pageName, 'success', resp.success);

            sharedModule.emptyForm($formName);

            $self.getStaffs();
            $modalName.modal('hide');

        }

        //error
        function error(e) {

            if (e.responseJSON.errors) {
                sharedModule.validation_errors($formName, e.responseJSON.errors);
            } else if (e.responseJSON.error) {
                sharedModule.alertMessage($formName, 'error', e.responseJSON.error);

            } else {
                console.log(e.statusText)
            }
        }
    }
    this.update = function (formInput, id) {
        var formData = formInput;
        $.post(base_url + '/admin/staff/update/' + id, formData).done(success).fail(error);

        //success
        function success(resp) {

            sharedModule.alertMessage($pageName, 'success', resp.success);

            sharedModule.emptyForm($formName);

            $self.getStaffs();
            $modalName.modal('hide');

        }

        //error
        function error(e) {

            if (e.responseJSON.errors) {
                sharedModule.validation_errors($formName, e.responseJSON.errors);
            } else if (e.responseJSON.error) {
                sharedModule.alertMessage($formName, 'error', e.responseJSON.error);

            } else {
                console.log(e.statusText)
            }
        }
    }
    this.edit = function (id) {
        $.get(base_url + '/admin/staff/' + id + '/edit').done(success).fail((err)=>console.log('Error to get Selected my team '));
        function success(resp) {

            var data = resp;
            $formName.find('input[name="id"]').val(data.id);

            $formName.find('input[name="name"]').val(data.name);
            $formName.find('select[name="position"]').val(data.position);
            $formName.find('textarea[name="information"]').val(data.information);


            //image
            $formName.find('input#thumbnail').val(data.profile_img);
            $formName.find('img#holder').attr('src', data.profile_img);
            if ($formName.find('input[name="profile_img"]').val() == "") {
                $formName.find('.removeImgBtn').addClass('hidden');
            } else {
                $formName.find('.removeImgBtn').removeClass('hidden');
            }

            //

            $formName.find('input[name="status"][value="' + data.status + '"]').prop("checked", true);
            $formName.find('button[type="submit"]').text('Update');
            $modalName.find('.modal-title').text('Edit My Team Page  Form');


        }
    }
    this.delete = function (id) {

        $.get(base_url + '/admin/staff/delete/' + id).done(success).fail(error);
        //success
        function success(resp) {

            sharedModule.alertMessage($pageName, 'success', resp.success);
            $self.getStaffs();


        }

        //error
        function error(e) {

            if (e.responseJSON.error) {
                sharedModule.alertMessage($formName, 'error', e.responseJSON.error);

            } else {
                console.log(e.statusText)
            }
        }
    }

    this.showStaffPage = function (selectorPage) {


        selectorPage.hide('slide', {direction: "right"}, 500, function () {
            $pageName.show();
            $self.getStaffs();
        });
    }


    //initilaize  function
    this.init = function () {


        $self.getStaffs();
        $self.bind();
    }();
}



