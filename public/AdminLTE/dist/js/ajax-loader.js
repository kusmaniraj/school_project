$(function () {
    var loaderHtml = '<div id="loader-wrapper" style="display: none"><div id="loader" >' +
        '<h1 data-text="Loading…">Loading…</h1>' +
        '</div> </div>';
    $('.content').prepend(loaderHtml);
})
$(document).ajaxStart(function () {
    $("#loader-wrapper").show()
});
$(document).ajaxComplete(function () {
    $("#loader-wrapper").hide();
});
$(document).ajaxError(function () {
    $("#loader-wrapper").hide()
});
