<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('company_name');
            $table->string('company_short_name');
            $table->string('company_slogan')->nullable();
            $table->string('logo_img')->nullable();
            $table->string('favicon_img')->nullable();
            $table->longText('google_map_iframe');
            $table->string('email');
            $table->string('address');
            $table->string('phone_number');
            $table->string('fax_number')->nullable();

            $table->longText('facebook_url')->nullable();
            $table->longText('skype_url')->nullable();
            $table->longText('google_plus_url')->nullable();
            $table->longText('twitter_url')->nullable();
            $table->string('company_version');
            $table->string('company_start_date');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
