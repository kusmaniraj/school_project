@extends('layouts.admin')

@section('content')



<!-- Main content -->
<section class="content">


    <div class="col-md-4">

        <!-- /.info-box -->
        <div class="info-box bg-green">
            <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Clients Visitor</span>
                <span class="info-box-number">92,050</span>

            </div>
            <!-- /.info-box-content -->
        </div>

        <!-- /.info-box -->
        <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Customer Messages</span>
                <span class="info-box-number">@isset($countCustomerMessages){{$countCustomerMessages}}@endisset</span>



            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->

    </div>



</section>
<!--main content-->

@endsection
@push('scripts')
<script src="https://adminlte.io/themes/AdminLTE/bower_components/chart.js/Chart.js"></script>
<script src="{{asset('build/pages/admin/dashboard.js')}}"></script>
@endpush

