@extends('layouts.admin')
@push('styles')
<link rel="stylesheet" href="https://plugins.krajee.com/assets/2a096422/css/fileinput.css">
<link rel="stylesheet" href="https://plugins.krajee.com/assets/fc69cbca/css/dropdown.min.css">
<link rel="stylesheet"
      href="https://adminlte.io/themes/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">


@endpush
@section('content')
    <section class="content-header">
        <h1>
            {{@$title}}
            <small>{{@$subTitle}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('gallery.index')}}">{{@$title}}</a></li>
            <li class="active">{{@$subTitle}}</li>
        </ol>

    </section>
    <section class="content" id="formGallery">
        @include('admin.message.alertMessage')
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><strong>Albums Form</strong></h3>
            </div>
            <div class="box-body">
                <form id="gallery_form" method="post"
                      action="{{@$editGallery['id'] ? route('gallery.update',$editGallery['id']): route('gallery.store')}}"
                      enctype="multipart/form-data">
                    @csrf


                    <div class="row form-group">
                        <div class="col-md-3">
                            <label for="name">Album Name <span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="albumName" class="form-control"
                                   value="{{@$editGallery['albumName'] ? $editGallery['albumName']: old('albumName')}}">
                        </div>

                    </div>


                    <div class="row form-group">
                        <div class="col-md-3">
                            <label for="title">Upload Date<span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input id="uploadDatepicker" data-date-format="yyyy-mm-dd" type="text"
                                       class="form-control pull-right" name="uploadDate"
                                       value="{{@$editGallery['uploadDate'] ? $editGallery['uploadDate']: old('uploadDate')}}">
                            </div>

                        </div>

                    </div>


                    <div class="row form-group">
                        <div class="col-md-3">
                            <label for="description">Description <span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <textarea name="description" id="" cols="100"
                                      rows="10">{{@$editGallery['description'] ? $editGallery['description']: old('description')}}</textarea>
                        </div>

                    </div>

                    <div class="row form-group">
                        <div class="col-md-3">
                            <label for="listImages">Images<span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">

                            @if(isset($editGallery->featuredImg) || $editGallery->featuredImg !='')
                            <input name="images[]" type="hidden" value="{{$editGallery->featuredImg}}">
                            <a href="{{route('image.showAlbumImages',$editGallery->id)}}" class="btn btn-xs btn-primary">View Images</a>&nbsp;<span class="label label-info">{{@$countImages}}</span>
                            @else
                                <div class="file-loading">
                                    <input id="listImages" name="images[]" type="file" multiple
                                           data-browse-on-zone-click="true">
                                </div>

                            @endif


                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label for="status">Status</label>
                        </div>
                        <div class=" col-md-9">
                            <div class="col-md-3">
                                <input type="radio" value="active" checked name="status" {{@$editGallery['status']=='active'
                            ? 'checked':'' }}> &nbsp;Active
                            </div>
                            <div class="col-d-3">
                                <input type="radio" value="inactive" name="status" {{@$editGallery['status']=='inactive' ?
                            'checked':'' }}>&nbsp;InActive
                            </div>


                        </div>

                    </div>
                    <div class="row form-group">

                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-primary">Created</button>

                        </div>

                    </div>

                </form>
            </div>
        </div>
    </section>


@endsection
@push('scripts')
<script src="https://plugins.krajee.com/assets/2a096422/js/fileinput.js"></script>
<script src="https://plugins.krajee.com/assets/fc69cbca/js/dropdown.min.js"></script>

<script>
    $(document).ready(function () {
        $("#listImages").fileinput({
            showUpload: false,
            dropZoneEnabled: true,
            allowedFileExtensions: ["jpg", "png"]

        });

    });
</script>

<!--datepicker-->
<script
    src="https://adminlte.io/themes/AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script> //Date picker
    $('#uploadDatepicker').datepicker({
        autoclose: true,

    })</script>
@endpush
