@extends('layouts.admin')
@push('styles')
<link rel="stylesheet" href="https://plugins.krajee.com/assets/2a096422/css/fileinput-rtl.min.css">
<link rel="stylesheet" href="https://plugins.krajee.com/assets/2a096422/css/fileinput.css">
<link rel="stylesheet" href="https://plugins.krajee.com/assets/fc69cbca/css/dropdown.min.css">

<style>
    .img-preview img {
        height: 200px;
        width: 200px;
    }

    .img-preview p {
        margin-top: 5px;
        text-align: center;
        width: 200px;
    }


</style>
@endpush
@section('content')


<!-- Main content -->
<section class="content-header">
    <h1>
        {{@$title}}
        <small>{{@$subTitle}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('gallery.index')}}">{{@$title}}</a></li>
        <li class="active">{{@$subTitle}}</li>
    </ol>

</section>
<section class="content " id="list_gallery">
    @include('admin.message.alertMessage')

    <!--  Form gallery box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-md-10">
                    <h3 class="box-title"><strong>Upload Images Form</strong></h3>
                </div>
                <div class="col-md-2 text-right">
                    <a href="{{route('gallery.edit',$albumId)}}" class="btn btn-sm btn-info"><i class="fa fa-arrow-left"></i>&nbsp;Back Edit</a>
                </div>
            </div>





        </div>
        <div class="box-body">
            <form action="{{route('image.storeAlbumImages',$albumId)}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="albumId" value="{{$albumId}}">
                @csrf

                <div class="row">
                    <div class="col-md-12">
                        <label for="listImages">File Gallery</label>

                        <div class="file-loading">
                            <input id="listImages" name="images[]" type="file" multiple
                                   data-browse-on-zone-click="true">
                        </div>

                    </div>
                </div>
            </form>
        </div>

    </div>
    <!--  End of form gallery box -->
    <!--  list gallery box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><strong>Gallery Albums List</strong></h3>

        </div>
        <div class="box-body">
            <div class="row">
                @isset($listImages)
                @foreach($listImages as $i=>$data)
                <div class="col-md-3">
                    <div class="img-preview" style="position: relative">
                        <img src="{{$data->image}}" alt="image-{{$i+1}}" class="img-thumbnail" ">
                        <p class="text-center">
                        <span>

                            &nbsp;
                             @if($data->Album->featuredImg!=$data->image)
                             <a href="{{route('image.deleteImage',[$data->id])}}" class="btn btn-xs btn-danger delete"><i class="fa fa-trash"></i></a>
                            <a href="{{route('image.changeCoverImage',[$data->albumId,$data->id])}}" class="changeCover btn btn-xs btn-info">Set Cover Image</a>
                            @else
                               <a href="#" class="btn btn-xs btn-success">Active Cover Image</a>
                            @endisset
                    </span>
                        </p>
                    </div>

                </div>
                @endforeach
                @endisset
            </div>
        </div>

    </div>
    <!--  End of list gallery box -->

</section>


@endsection
@push('scripts')
<script src="https://plugins.krajee.com/assets/2a096422/js/fileinput.js"></script>
<script src="https://plugins.krajee.com/assets/fc69cbca/js/dropdown.min.js"></script>
<script>
    $(document).ready(function () {
        $("#listImages").fileinput({
            showUpload: true,
            dropZoneEnabled: true,
            allowedFileExtensions: ["jpg", "png"]

        });

    });

    $('.changeCover').on('click', function (e) {

        e.preventDefault();
        showAlertBox('Change Album Cover',this);

    });
</script>


@endpush
