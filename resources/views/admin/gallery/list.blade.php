@extends('layouts.admin')
@push('styles')

<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush
@section('content')


<!-- Main content -->
<!--        content header-->
<section class="content-header">
    <h1>
        {{@$title}}<small>{{@$subTitle}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{@$title}}</li>
    </ol>

</section>
<section class="content " id="list_gallery">
    @include('admin.message.alertMessage')
    <!--  box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><strong>Gallery Albums List</strong></h3>
            <a href="{{route('gallery.show')}}" id="add_gallery_btn" class="btn btn-primary pull-right">Add Gallery Album </a>
        </div>
        <div class="box-body">
            <table id="gallery_table" class="table">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Album Name</th>
                    <th>Description</th>
                    <th>Upload Date</th>
                    <th>Covered Image</th>
                    <th>Images</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @isset($listGalleries)
                @foreach($listGalleries as $key=>$data)
                <tr>
                   <td>{{$key+1}}</td>
                    <td>{{$data->albumName}}</td>
                    <td>{{$data->description}}</td>
                    <td>{{$data->uploadDate}}</td>
                    <td><img src="{{$data->featuredImg}}" alt="" style="width: 50px;height: 50px"></td>
                    <td><a href="{{route('image.showAlbumImages',$data->id)}}" class="btn btn-xs btn-primary">View</a>&nbsp;<span class="label label-info">{{count($data->AlbumImages)}}</span></td>
                    <td>
                        <a href="{{route('gallery.delete',$data->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></a>
                        &nbsp;
                        <a href="{{route('gallery.edit',$data->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>

                    </td>
                </tr>
                @endforeach
                @endisset

                </tbody>


            </table>
        </div>
        <!-- /.box-body -->
    </div>


</section>


@endsection
@push('scripts')
<!--datatable js-->
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
    $('#gallery_table').dataTable();
</script>


@endpush
