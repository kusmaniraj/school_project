

@if(Session::has('success'))
<div class="alert alert-success alert-status">
    <strong>Successfully !!</strong>&nbsp;&nbsp;{{Session::get('success')}}
</div>
@push('scripts')
<script>
    alertify.success('Success: ' + "{{Session::get('success')}}");
</script>
@endpush
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-status">
    <strong>Error !!</strong>&nbsp;&nbsp;{{Session::get('error')}}
</div>
@push('scripts')
<script>
    alertify.error('Error: ' + "{{Session::get('error')}}");
</script>
@endpush
@endif


@if (count($errors) > 0)
<div class = "alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@push('scripts')
<script>
    window.setTimeout(function() {
        $('.alert-status').slideUp(800,function(){
            $(this).remove();
        });
    },2000)

</script>


@endpush
