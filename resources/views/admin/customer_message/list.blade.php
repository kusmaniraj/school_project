@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush
@section('content')


<!-- Main content -->
<section class="content " id="page_message">

    <!--  box -->
    <div class="box box-primary">


        <div class="box-body">


            <div class="box-header with-border">
                <h3 class="box-title"><strong>Customer Message List</strong></h3>

            </div>
            <div class="box-body">
              <table id="message_table" class="table">
                  <thead>
                  <tr>
                      <th>SN</th>
                      <th>Name</th>
                      <th>Message</th>

                      <th>Status</th>


                      <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @isset($messages)
                    @foreach($messages as $key=>$message)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$message->name}}</td>
                            <td>{{$message->message}}</td>

                            <td>
                                <span class="label label-{{($message->status='seen') ? 'success':'danger'}}">{{$message->status}}</span></td>
                            <td>
                                <a href="{{route('message.delete',$message->id)}}" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>

                    @endforeach
                  @endisset

                  </tbody>


              </table>
            </div>


        </div>
        <!-- /.box-body -->


    </div>


</section>
@endsection

@push('scripts')



<!--datatable js-->
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
    $('#message_table').DataTable();
    $('.remove').on('click',function(){
        if(confirm('Are you sure want to Delete ?')==true){
            return true;
        }else{
            return false;
        }
    })
</script>



@endpush
