@extends('layouts.admin')



@section('content')
<!-- Main content -->
<!--Content Header-->
<section class="content-header">
    <h1>
        {{@$title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{@$title}}</li>
    </ol>

</section>
<!--End of Content Header-->

<section class="content">
    <form role="form" method="post" action="{{@$setting->id ? route('setting.store',$setting->id) :route('setting.store')}}">
<!--        alert message-->
        @include('admin.message.alertMessage')
        {{csrf_field()}}
        <!--  box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">School Info</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('company_name') ? ' has-error' : '' }} ">
                            <label for="companyName">School Name &nbsp;<span
                                    class="required text-danger">*</span></label>
                            <input type="text" class="form-control " id="companyName" placeholder="Enter School Name"
                                   name="company_name" value="{{@$setting->company_name ? $setting->company_name :old('company_name')}}" >
                            @if ($errors->has('company_name'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('company_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('company_short_name') ? ' has-error' : '' }} ">
                            <label for="companyName">School Short Name &nbsp;<span
                                    class="required text-danger">*</span></label>
                            <input type="text" class="form-control " id="companyShortName" placeholder="Enter School Short Name"
                                   name="company_short_name" value="{{@$setting->company_short_name ? $setting->company_short_name :old('company_short_name')}}" >
                            @if ($errors->has('company_short_name'))
                                <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('company_short_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('company_slogan') ? ' has-error' : '' }}">
                            <label for="companySlogan">School Slogan</label>
                            <input type="text" class="form-control" id="companySlogan"
                                   placeholder="Enter School Slogan" name="company_slogan" value="{{@$setting->company_slogan ? $setting->company_slogan :old('company_slogan')}}">
                            @if ($errors->has('company_slogan'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('company_slogan') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email &nbsp;<span class="required text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" placeholder="Enter School Email"
                                   name="email" value="{{@$setting->email ? $setting->email :old('email')}}">
                            @if ($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="">Address&nbsp;<span class="required text-danger">*</span></label>
                            <input type="text" class="form-control" id="companyAddress"
                                   placeholder="Enter School Address" name="address" value="{{@$setting->address ? $setting->address :old('address')}}">
                            @if ($errors->has('address'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('address') }}</strong>
                                    </span>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="companyPhoneNo">Phone Number&nbsp;<span
                                    class="required text-danger">*</span></label>
                            <input type="number" class="form-control" id="companyPhoneNo"
                                   placeholder="Enter School Phone Number" name="phone_number" value="{{@$setting->phone_number ? $setting->phone_number :old('phone_number')}}">

                            @if ($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('phone_number') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group ">
                            <label for="companyFaxNo">Fax</label>
                            <input type="number" class="form-control" id="companyFax" placeholder="Enter SchoolFax"
                                   name="fax_number" value="{{@$setting->fax_number ? $setting->fax_number :old('fax_number')}}">
                        </div>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->


        </div>
        <!-- logo /.box -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Logo Image&nbsp;<span
                        class="required text-danger">*</span></h3>
            </div>
            <div class="box-body ">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('logo_img') ? ' has-error' : '' }}">
                            <label for="featured_img">Logo Image</label>
                                        <span class="input-group-btn">
                                                 <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-info">
                                                     <i class="fa fa-picture-o"></i> Choose Logo Image
                                                 </a>
                                               </span>

                            <p class="help-block">Please Upload Logo Size of 120*300 px</p>
                            @if ($errors->has('logo_img'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('logo_img"') }}</strong>
                                    </span>
                            @endif
                            <input id="thumbnail" class="form-control" type="hidden" name="logo_img" value="{{@$setting->logo_img ? $setting->logo_img :old('logo_img')}}">
                            <img id="holder" style="margin-top:15px;max-height:100px;" src="{{@$setting->logo_img ? $setting->logo_img :old('logo_img')}}">
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('favicon_img') ? ' has-error' : '' }}">
                            <label for="favicon_img">Logo Image</label>
                                        <span class="input-group-btn">
                                                 <a id="lfm-2" data-input="thumbnail-2" data-preview="holder-2" class="btn btn-info">
                                                     <i class="fa fa-picture-o"></i> Choose Favicon Image
                                                 </a>
                                               </span>

                            <p class="help-block">Please Upload Logo Size of 20*20 </p>
                            @if ($errors->has('favicon_img'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('favicon_img"') }}</strong>
                                    </span>
                            @endif
                            <input id="thumbnail-2" class="form-control" type="hidden" name="favicon_img" value="{{@$setting->company_name ? $setting->favicon_img :old('favicon_img')}}">
                            <img id="holder-2" style="margin-top:15px;max-height:100px;" src="{{@$setting->favicon_img ? $setting->favicon_img :old('favicon_img')}}">
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!--end of logo box-->


        <!-- Iframe /.box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Iframe Src</h3>
            </div>
            <div class="box-body ">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group ">
                            <label for="exampleInputFile">Google Maps Iframe</label>
                            <textarea name="google_map_iframe" id="" cols="30" rows="10"
                                      class="form-control">{{@$setting->google_map_iframe ? $setting->google_map_iframe :old('google_map_iframe')}}</textarea>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end of iframe box-->

        <!-- social /.box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Social Url </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group ">
                            <label for="facebook_url">Facebook Url</label>
                            <input type="text" class="form-control" id="facebook_url" placeholder="Enter Facebook Url"
                                   name="facebook_url" value="{{@$setting->facebook_url ? $setting->facebook_url :old('facebook_url')}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group ">
                            <label for="google_plus_url">Google Plus Url</label>
                            <input type="text" class="form-control" id="google_plus_url"
                                   placeholder="Enter Google Plus Url"
                                   name="google_plus_url" value="{{@$setting->google_plus_url ? $setting->company_name :old('google_plus_url')}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group ">
                            <label for="skype_url">Skype Url</label>
                            <input type="text" class="form-control" id="google_plus_url" placeholder="Enter Skype Url"
                                   name="skype_url" value="{{@$setting->skype_url ? $setting->skype_url :old('skype_url')}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group ">
                            <label for="twitter_url">Twitter Url</label>
                            <input type="text" class="form-control" id="twitter_url" placeholder="Enter Twitter Url"
                                   name="twitter_url" value="{{@$setting->twitter_url ? $setting->twitter_url :old('twitter_url')}}">
                        </div>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->


        </div>
        <!-- end of social box-->

        <div class="box ">
            <div class="box-body ">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('company_version') ? ' has-error' : '' }}">
                            <label for="exampleInputFile"><span
                                    class="required text-danger">*</span>&nbsp;Website Version</label>
                            <input type="text" name="company_version" class="form-control" value="{{@$setting->company_version ? $setting->company_version :old('company_version')}}">
                            @if ($errors->has('company_version'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('company_version') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('company_start_date') ? ' has-error' : '' }}">
                            <label for="exampleInputFile"><span
                                    class="required text-danger">*</span>&nbsp;Website Start date</label>
                            <input type="date" name="company_start_date" class="form-control" value="{{@$setting->company_start_date ? $setting->company_start_date :old('company_start_date')}}">
                            @if ($errors->has('company_start_date'))
                                    <span class="help-block" role="alert">
                                        <strong >{{ $errors->first('company_start_date') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">{{@$setting->id ? 'Update':'Create'}}</button>
            </div>
        </div>
    </form>
</section>
<!--main content-->

@endsection
@push('scripts')
<!--filemanager-->
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<!--standalone button-->
<script>
    $('#lfm').filemanager('file');
</script>
<script>
    $('#lfm-2').filemanager('file');
</script>
@endpush

