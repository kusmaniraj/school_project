@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush
@section('content')


<!-- Main content -->
<!--Content Header-->
<section class="content-header">
    <h1>
        {{@$title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="{{route('studentReview.index')}}">{{@$title}}</a></li>
        <li class="active">{{(@$editStudentReview) ? 'Edit': 'Add'}} Form</li>
    </ol>

</section>
<!--End of Content Header-->

<!--Content Body-->
<section class="content " id="studentReview-form">
    <!--  Alert Messages-->
    @include('admin.message.alertMessage')
    <!--    End of Alert Messages-->
    <!--  box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><strong>  Student Review Form</strong></h3>
        </div>
        <div class="box-body">
            <form id="studentReview_form" method="post"
                  action="{{(@$editStudentReview->id) ? route('studentReview.update',@$editStudentReview->id) : route('studentReview.store')}}">
                @csrf
                <input type="hidden" name="id" value="{{@$editStudentReview->id}}">

                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="name">Title <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" name="title" class="form-control"
                               value="{{(@$editStudentReview->title) ? $editStudentReview->title : old('title')  }}">
                    </div>

                </div>

                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Description <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-9">
                        <textarea name="description" id="" cols="100" rows="10">{{(@$editStudentReview->description) ? $editStudentReview->description : old('description')}}</textarea>
                    </div>

                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Featured Image <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-9">
                         <span class="input-group-btn">
                                                 <a id="lfm" data-input="thumbnail" data-preview="holder"
                                                    class="btn btn-info">
                                                     <i class="fa fa-picture-o"></i> Choose Featured Image
                                                 </a>
                                               </span>
                        <input id="thumbnail" class="form-control" type="hidden" name="profileImg"
                               value="{{(@$editStudentReview->profileImg) ? $editStudentReview->profileImg : old('profileImg')}}">
                        <img id="holder" style="margin-top:15px;max-height:100px;"
                             src="{{(@$editStudentReview->profileImg) ? $editStudentReview->profileImg : old('profileImg')}}">
                        <a class="removeImgBtn btn btn-sm btn-danger " style="display: none">Remove</a>


                    </div>

                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="name">Student Name <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="name" class="form-control"
                               value="{{(@$editStudentReview->name) ? $editStudentReview->name : old('name')  }}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="name">Academic<span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-3">
                        <select name="academic" id="academic" class="form-control">
                            <option value="" selected disabled>Select Academic</option>
                            @isset($academicsList)
                            @foreach($academicsList as $academic)
                            <option value="{{$academic->title}}" {{(@$editStudentReview->academic==$academic->title || old('academic')
                                ==$academic->title) ? 'selected' : '' }}> {{$academic->title}}
                            </option>
                            @endforeach
                            @endisset
                        </select>

                    </div>
                    <div class="col-md-2">
                        <label for="grade">Grade <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-2">
                        <input type="number" name="grade" class="form-control"
                               value="{{(@$editStudentReview->grade) ? $editStudentReview->grade : old('grade')  }}">
                    </div>

                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="status">Status</label>
                    </div>
                    <div class=" col-md-9">
                        <div class="col-md-3">
                            <input id="activeStatus" type="radio" value="active" name="status" {{(@$editStudentReview->status=='active')
                            ? 'checked' : '' }} checked > &nbsp;<label for="activeStatus">Active</label>
                        </div>
                        <div class="col-d-3">
                            <input id="inActiveStatus" type="radio" value="inactive" name="status" {{(@$editStudentReview->status=='inactive')
                            ? 'checked' : '' }}>&nbsp;<label for="inActiveStatus">InActive</label>
                        </div>


                    </div>

                </div>
                <div class="box-footer">

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right">{{(@$editStudentReview) ? 'Updated':
                            'Created'}}
                        </button>
                        <a href="{{route('studentReview.index')}}" class="btn btn-default">Close</a>
                    </div>

                </div>

            </form>
        </div>


    </div>
</section>

<!--End Content Body-->
<!--main content-->

@endsection


@push('scripts')

<!--filemanager-->
<script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
<!--standalone button-->
<script>
    $('#lfm').filemanager('file');
</script>
<!--Image Holder/hide and show image-->
<script src="{{asset('/build/pages/admin/shared/ImageHolder.js')}}"></script>
<script>
    let imageHolder = new ImageHolder('input[name="profileImg"]', '#holder', '.removeImgBtn');
</script>

<!--ck editor-->
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>


@endpush
