@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush
@section('content')


<!-- Main content -->
<!--Content Header-->
<section class="content-header">
        <h1>
            {{@$title}}
        </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">{{@$title}} List</li>
    </ol>

</section>
<!--End of Content Header-->
<section class="content " id="page_studentReview">
    <!--  Alert Messages-->
    @include('admin.message.alertMessage')
    <!--    End of Alert Messages-->
    <!--  box -->
    <div class="box box-primary">


        <div class="box-body">


            <div class="box-header with-border">
                <h3 class="box-title"><strong>List of Student Reviews </strong></h3>
                <a href="{{route('studentReview.show')}}" id="add_studentReview_btn" class="btn btn-primary pull-right">Add
                    Student Review </a>
            </div>
            <div class="box-body">
                <table id="studentReview_table" class="table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Academic</th>
                        <th>Grade</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @isset($listStudentReviews)
                    @foreach($listStudentReviews as $key=>$studentReview)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$studentReview->title}}</td>
                        <td>{{$studentReview->description}}</td>
                        <td>{{$studentReview->academic}}</td>
                        <td>{{$studentReview->grade}}</td>
                        <td>
                            <span class="label {{($studentReview->status =='active') ? 'label-success':'label-danger'}}">{{$studentReview->status}}</span>
                        </td>

                        <td>
                            <a href="{{route('studentReview.delete',$studentReview->id)}}"
                               class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></a>
                            &nbsp;
                            <a href="{{route('studentReview.edit',$studentReview->id)}}" class="btn btn-primary btn-xs"><i
                                    class="fa fa-edit"></i></a>

                        </td>

                    </tr>
                    @endforeach
                    @endisset

                    </tbody>


                </table>
            </div>


        </div>
        <!-- /.box-body -->


    </div>


</section>
<!--main content-->

@endsection
@push('scripts')

<!--datatable js-->
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
    $('#studentReview_table').dataTable();
</script>


@endpush
