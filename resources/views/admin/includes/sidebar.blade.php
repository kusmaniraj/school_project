<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('AdminLTE/dist/img/avatar.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ucfirst(Auth::user()->name )}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <!--            Dashboard-->
            <li class="@isset($title){{$title=='Dashboard' ? 'active': ''}} @endisset"><a
                    href="{{route('dashboard.index')}}"><i class="fa  fa-dashboard"></i> <span>Dashboard</span></a></li>

              <li class="@isset($title){{$title=='Menu' ? 'active': ''}} @endisset"><a href="{{route('menu.index')}}"><i
                      class="fa  fa-list"></i> <span>Menu Management</span></a></li>
            <!--Slider-->
            <li class="@isset($title){{$title=='Slider' ? 'active': ''}} @endisset"><a
                    href="{{route('slider.index')}}"><i class="fa  fa-folder"></i>
                    <span>Sliders</span></a></li>
            <!--Page-->
            <li class="header">PAGE</li>
            <li class="treeview @isset($parentTitle){{$parentTitle=='Page' ? 'active': ''}} @endisset">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Page Management</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@isset($title){{$title=='About_Us' ? 'active': ''}} @endisset"><a
                            href="{{route('page.index','About_Us')}}"><i class="fa fa-circle-o"></i>About Us</a></li>
                    <li class="@isset($title){{$title=='Notice' ? 'active': ''}} @endisset"><a
                            href="{{route('page.index','Notice')}}"><i class="fa fa-circle-o"></i>Notices</a></li>
                    <li class="@isset($title){{$title=='Event' ? 'active': ''}} @endisset"><a
                            href="{{route('page.index','Event')}}"><i class="fa fa-circle-o"></i>Events</a></li>
                    <li class="@isset($title){{$title=='News' ? 'active': ''}} @endisset"><a
                            href="{{route('page.index','News')}}"><i class="fa fa-circle-o"></i>News</a></li>
                    <li class="@isset($title){{$title=='Admission' ? 'active': ''}} @endisset"><a
                            href="{{route('page.index','Admission')}}"><i class="fa fa-circle-o"></i>Admissions</a></li>
                    <li class="@isset($title){{$title=='Academic' ? 'active': ''}} @endisset"><a
                            href="{{route('page.index','Academic')}}"><i class="fa fa-circle-o"></i>Academics</a></li>



                </ul>
            </li>
            <!--Student review            -->
            <li class="@isset($title){{$title=='Student Reviews' ? 'active': ''}} @endisset"><a
                    href="{{route('studentReview.index')}}"><i class="fa  fa-group"></i>
                    <span>Student Reviews</span></a></li>

            <!--My Staffs-->
            <li class="@isset($title){{$title=='Leaderships' ? 'active': ''}} @endisset"><a
                    href="{{route('leadership.index')}}"><i class="fa  fa-users"></i> <span>Leaderships</span></a></li>
            <li class="@isset($title){{$title=='Gallery Album' ? 'active': ''}} @endisset"><a
                    href="{{route('gallery.index')}}"><i class="fa  fa-image"></i>
                    <span>Gallery Album</span></a></li>
            <!--Customer Message-->
            <li class="header">Messages</li>
            <li class="@isset($title){{$title=='Customer Messages' ? 'active': ''}} @endisset"><a
                    href="{{route('message.index')}}"><i class="fa  fa-envelope"></i> <span>Customer Messages</span></a>
            </li>
            <!--FileManager-->
            <li class="header">File Manager</li>
            <li class="@isset($title){{$title=='File Manager' ? 'active': ''}} @endisset"><a
                    href="{{route('admin.file_manager')}}"><i class="fa  fa-folder-open-o"></i>
                    <span>File Manager</span></a></li>
            <!--Setting-->
            <li class="header">SETTING</li>
            <li class="@isset($title){{$title=='General Setting' ? 'active': ''}} @endisset"><a
                    href="{{route('setting')}}"><i class="fa  fa-gears"></i> <span>General Setting</span></a></li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
