<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> {{config('setting.company_version')}}
    </div>
    <strong>Copyright &copy;{{date('Y',strtotime(config('setting.company_start_date')))}}-{{date('Y')}} <a href="{{url('/')}}">{{config('setting.company_name')}}</a>.</strong> All rights
    reserved.
</footer>
