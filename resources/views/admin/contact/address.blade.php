<div class="row form-group">
    <div class="col-md-3">
        <label for="country">Country <span class="required text-danger">*</span></label>
    </div>
    <div class="col-md-9">
        <input type="text" name="Country" class="form-control">
    </div>

</div>


<div class="row form-group">
    <div class="col-md-3">
        <label for="state">State <span class="required text-danger">*</span></label>
    </div>
    <div class="col-md-9">
        <input type="text" name="State" class="form-control">
    </div>

</div>
<div class="row form-group">
    <div class="col-md-3">
        <label for="City">City <span class="required text-danger">*</span></label>
    </div>
    <div class="col-md-9">
        <input type="text" name="City" class="form-control">
    </div>

</div>
