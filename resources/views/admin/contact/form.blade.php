<div class="modal" id="contact_modal_form" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">Add {{@$title}}  Form</h5>

            </div>
            <div class="modal-body">
                <form id="contact_form">
                    <input type="hidden" name="id">
                    <input type="hidden" name="contactType" value="{{@$contactType}}">

                    <div class="row form-group">
                        <div class="col-md-3">
                            <label for="title"> Title <span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="title" class="form-control">
                        </div>

                    </div>



                    @if($contactType=='Enquiry')
                    @include('admin.contact.enquiry')
                    @endif
                    @if($contactType=='Address')
                    @include('admin.contact.address')
                    @endif


                    <div class="row form-group">
                        <div class="col-md-3">
                            <label for="status">Status</label>
                        </div>
                        <div class=" col-md-9">
                            <div class="col-md-3">
                                <input type="radio" value="active" checked name="status" id="active"> &nbsp;<label for="active">Active</label>
                            </div>
                            <div class="col-d-3">
                                <input type="radio" value="inactive" name="status" id="InActive">&nbsp;<label
                                    for="InActive">InActive</label>
                            </div>


                        </div>

                    </div>
                    <div class="row form-group">

                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-primary">Created</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

@push('scripts')

<!--filemanager-->
<script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
<!--standalone button-->
<script>
    $('#lfm').filemanager('file');
</script>

<!--ck editor-->
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script>
    //    var options = {
    //        filebrowserImageBrowseUrl: '/laravel-filemanager?type=file',
    //        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    //        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    //        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    //    };
    //    $('textarea').ckeditor(options);
    //
    $('textarea').ckeditor();
</script>

@endpush
