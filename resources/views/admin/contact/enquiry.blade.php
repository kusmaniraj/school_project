<div class="row form-group">
    <div class="col-md-3">
        <label for="date">Enquiry Email <span class="required text-danger">*</span></label>
    </div>
    <div class="col-md-9">
        <input type="email" name="email" class="form-control">
    </div>

</div>

<div class="row form-group">
    <div class="col-md-3">
        <label for="phone">Enquiry Phone <span class="required text-danger">*</span></label>
    </div>
    <div class="col-md-9">
        <input type="text" name="phone" class="form-control" >
    </div>

</div>


