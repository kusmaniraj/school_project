@extends('admin::layouts.admin')

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endpush
@section('content')


<!-- Main content -->
<section class="content " id="contact">

    <!--  box -->
    <div class="box box-primary">


        <div class="box-body">


            <div class="box-header with-border">
                <h3 class="box-title"><strong> {{ucFirst($contactType)}} Contact List</strong></h3>
                <a href="#" id="add_contact_btn" class="btn btn-primary pull-right">Add {{ucFirst($contactType)}}  </a>

            </div>
            <div class="box-body">
              <table id="contact_table" class="table" data-contact="{{$contactType}}">
                  <thead>
                  <tr>
                      <th>SN</th>
                      <th>Title</th>
                      <th>Status</th>

                      <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>


              </table>
            </div>


        </div>
        <!-- /.box-body -->


    </div>


</section>
<!--main content-->
<!--modal form-->
@include('admin.contact.form')
@endsection
@push('scripts')


<!--datatable js-->
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>


<script src="{{asset('build/pages/admin/shared/shared.js')}}"></script>
<script src="{{asset('build/pages/admin/contact.js')}}"></script>
@endpush
