@extends('admin::layouts.admin')
@push('styles')
<!--filemanager-->
<link rel="stylesheet" href="{{asset('/vendor/laravel-filemanager/css/lfm.css')}}">
<!--standalone button-->
@endpush
@section('content')


<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Profile Information</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">

            <form role="form" method="post" action="{{route('profile.update',$admin->id)}}">
                <!--        alert message-->
                @include('admin::message.alertMessage')
                {{csrf_field()}}

                <div class="container">
                    <div class="row form-group {{ $errors->has('name') ? ' has-error' : '' }} ">
                        <div class="col-md-3">
                            <label for="name">Name &nbsp; <span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="name" class="form-control"
                                   value="@isset($admin) {{$admin->name}} @else {{old('name')}} @endisset">

                            @if ($errors->has('name'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group {{ $errors->has('email') ? ' has-error' : '' }} ">
                        <div class="col-md-3">
                            <label for="name">Email &nbsp; <span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="email" name="email" class="form-control"
                                   value="@isset($admin) {{$admin->email}} @else {{old('email')}} @endisset">
                            @if ($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group ">
                        <div class="col-md-3">
                            <label for="gender">Gender</label>
                        </div>
                        <div class="col-md-9">
                            <input type="radio" value="male"  name="gender" {{$admin->gender =='male' ? 'checked':''}}> &nbsp;Male
                            <input type="radio" value="female" name="gender"  {{$admin->gender =='female' ? 'checked':''}}>&nbsp;Female

                        </div>
                    </div>
                    <div class="row form-group  ">
                        <div class="col-md-3">
                            <label for="profile_image">Profile Image</label>
                        </div>
                        <div class="col-md-9">

                                        <span class="input-group-btn">
                                                 <a id="profile_img" data-input="thumbnail" data-preview="holder"
                                                    class="btn btn-info">
                                                     <i class="fa fa-picture-o"></i> Choose Profile Image
                                                 </a>
                                               </span>
                            <input id="thumbnail" class="form-control" type="hidden" name="profile_image">
                            <img id="holder" style="margin-top:15px;max-height:100px;">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="text-info"><strong>Note*&nbsp;</strong>Below fields are for changing password only.</h5>
                        </div>


                    </div>
                    <div class="row form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
                        <div class="col-md-3">
                            <label for="old_password">Old Password &nbsp; <span
                                    class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="password" name="old_password" class="form-control" value="">
                            @if ($errors->has('old_password'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-md-3">
                            <label for="password">Password &nbsp; <span class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="password" name="password" class="form-control" value="">
                            @if ($errors->has('password'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group {{ $errors->has('password') ? ' has-error' : '' }} ">
                        <div class="col-md-3">
                            <label for="password_confirmation">Confirm Password &nbsp; <span
                                    class="required text-danger">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="password" name="password_confirmation" class="form-control" value="">
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </div>
</section>
<!--main content-->

@endsection
@push('scripts')
<!--filemanager-->
<script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
<!--standalone button-->
<script>
    $('#profile_img').filemanager('file');
</script>

@endpush

