@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endpush
@section('content')


<!-- Main content -->
<!--Content Header-->
<section class="content-header">
    <h1>
        {{@$parentTitle}} <small>{{@$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{@$title}}</li>
    </ol>

</section>
<!--End of Content Header-->

<!--Content Body-->
<section class="content " id="page">
    <!--  Alert Messages-->
    @include('admin.message.alertMessage')
    <!--    End of Alert Messages-->
    <!--  box -->
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title"><strong> {{$title}} Page List</strong></h3>
                <a href="{{route('page.show',$pageType)}}" id="add_page_btn" class="btn btn-primary pull-right">Add Content  </a>

            </div>
            <div class="box-body">
                @include('admin.page.'.$pageType.'.list')
            </div>
    </div>

</section>

<!--End Content Body-->
<!--main content-->

@endsection
@push('scripts')

<!--datatable js-->
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
    $('#page_table').dataTable();
</script>

@endpush
