<table id="page_table" class="aboutus-table table" >
    <thead>
    <tr>
        <th>SN</th>
        <th>Title</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @isset($pageResults)
    @foreach($pageResults as $key=>$data)
    <tr>
        <td>{{$key+1}}</td>
        <td>{{$data->title}}</td>
        <td>
            <span class="label {{($data->status =='active') ? 'label-success':'label-danger'}}">{{$data->status}}</span>
        </td>
        <td>
            <a href="{{route('page.delete',$data->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></a>
            &nbsp;
            <a href="{{route('page.edit',$data->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>

        </td>
    </tr>
    @endforeach
    @endisset

    </tbody>


</table>
