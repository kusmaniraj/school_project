
<div class="row form-group">
    <div class="col-md-3">
        <label for="date"> Date <span class="required text-danger">*</span></label>
    </div>
    <div class="col-md-6">
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>


            <input id="uploadDatepicker" data-date-format="yyyy-mm-dd" type="text"
                   class="form-control pull-right" name="date"
                   value="{{(@$editPage->date) ? $editPage->date : old('date') }}">
        </div>
    </div>

</div>
<div class="row form-group">
    <div class="col-md-3">
        <label for="title">Featured Image</label>
    </div>
    <div class="col-md-9">
                         <span class="input-group-btn">
                                                 <a id="lfm" data-input="thumbnail" data-preview="holder"
                                                    class="btn btn-info">
                                                     <i class="fa fa-picture-o"></i> Choose Featured Image
                                                 </a>
                                               </span>
        <input id="thumbnail" class="form-control" type="hidden" name="featured_img" value="{{(@$editPage->featured_img) ? $editPage->featured_img : old('featured_img') }}">
        <img id="holder" src="{{(@$editPage->featured_img) ? $editPage->featured_img : old('featured_img') }}" style="margin-top:15px;max-height:100px;">
        <a class="removeImgBtn btn btn-sm btn-danger " style="display: none">Remove</a>
    </div>

</div>
