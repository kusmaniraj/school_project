@extends('layouts.admin')

@push('styles')
<link rel="stylesheet"
      href="https://adminlte.io/themes/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css">
@endpush
@section('content')


<!-- Main content -->
<!--Content Header-->
<section class="content-header">
    <h1>
        {{@$parentTitle}}
        <small>{{@$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="{{route('page.index',$pageType)}}">{{@$title}}</a></li>
        <li class="active">{{(@$editPage) ? 'Edit': 'Add'}} Form</li>
    </ol>

</section>
<!--End of Content Header-->

<!--Content Body-->
<section class="content " id="page-form">
    <!--  Alert Messages-->
    @include('admin.message.alertMessage')
    <!--    End of Alert Messages-->
    <!--  box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><strong> {{$title}} Page Form</strong></h3>


        </div>
        <form id="page_form" method="post"
              action="{{(@$editPage->id) ? route('page.update',@$editPage->id) : route('page.store')}}">
            @csrf
            <input type="hidden" name="id" value="{{@$editPage->id}}">
            <input type="hidden" name="page_type" value="{{$pageType}}">

            <div class="box-body">


                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Content Title <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="title" class="form-control"
                               value="{{(@$editPage->title) ? $editPage->title : old('title') }}">
                    </div>

                </div>

                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Description <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-9">
                        <textarea name="description" id="" cols="30" rows="10">
                            {{(@$editPage->description) ? $editPage->description : old('description') }}
                        </textarea>
                    </div>

                </div>

                <!--PAGE include-->
                @include('admin.page.'.$pageType.'.form')
                <!--PAGE include-->


                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="status">Status</label>
                    </div>
                    <div class=" col-md-9">
                        <div class="col-md-3">
                            <input type="radio" value="active" name="status" id="active" {{(@$editPage->status=='active')
                            ? 'checked' : '' }} checked > &nbsp;<label
                                for="active">Active</label>
                        </div>
                        <div class="col-d-3">
                            <input type="radio" value="inactive" name="status" id="InActive" {{(@$editPage->status=='inactive')
                            ? 'checked' : '' }}>&nbsp;<label
                                for="InActive">InActive</label>
                        </div>


                    </div>

                </div>


            </div>
            <div class="box-footer">

                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right">{{(@$editPage) ? 'Updated': 'Created'}}
                    </button>
                    <a href="{{route('page.index',$pageType)}}" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
                </div>

            </div>
        </form>
    </div>
</section>

<!--End Content Body-->
<!--main content-->

@endsection


@push('scripts')

<!--filemanager-->
<script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
<script>
    $('#lfm').filemanager('file');
</script>
<!--standalone button-->
<!--Image Holder/hide and show image-->
<script src="{{asset('/build/pages/admin/shared/ImageHolder.js')}}"></script>
<script>
    let imageHolder = new ImageHolder('input[name="featured_img"]', '#holder', '.removeImgBtn');
</script>

<!--ck editor-->
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script>

    $('textarea').ckeditor();
</script>
<!--datepicker-->
<script
    src="https://adminlte.io/themes/AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script> //Date picker
    $('#uploadDatepicker').datepicker({
        autoclose: true,

    })</script>

<!--TimePicker-->
<script src="https://adminlte.io/themes/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script>
    //Timepicker
    $('.timepicker').timepicker({
        showInputs: false
    })

</script>
@endpush
