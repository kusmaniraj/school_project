@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush
@section('content')


<!-- Main content -->
<!--Content Header-->
<section class="content-header">
    <h1>
        {{@$title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="{{route('slider.index')}}">{{@$title}}</a></li>
        <li class="active">{{(@$editSlider) ? 'Edit': 'Add'}} Form</li>
    </ol>

</section>
<!--End of Content Header-->

<!--Content Body-->
<section class="content " id="slider-form">
    <!--  Alert Messages-->
    @include('admin.message.alertMessage')
    <!--    End of Alert Messages-->
    <!--  box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><strong> Slider Page Form</strong></h3>


        </div>
        <form id="slider_form" method="post"
              action="{{(@$editSlider->id) ? route('slider.update',@$editSlider->id) : route('slider.store')}}">
            @csrf
            <input type="hidden" name="id" value="{{@$editSlider->id}}">


            <div class="box-body">


                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Content Title <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" name="title" class="form-control"
                               value="{{(@$editSlider->title) ? $editSlider->title : old('title') }}">
                    </div>

                </div>

                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Description <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-9">
                        <textarea name="description" id="" cols="30" rows="10">
                            {{(@$editSlider->description) ? $editSlider->description : old('description') }}
                        </textarea>
                    </div>

                </div>


                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Featured Image<span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-9">
                         <span class="input-group-btn">
                                                 <a id="lfm" data-input="thumbnail" data-preview="holder"
                                                    class="btn btn-info">
                                                     <i class="fa fa-picture-o"></i> Choose Featured Image
                                                 </a>
                                               </span>
                        <input id="thumbnail" class="form-control" type="hidden" name="featured_img"
                               value="{{(@$editSlider->featured_img) ? $editSlider->featured_img : old('featured_img') }}">
                        <img id="holder"
                             src="{{(@$editSlider->featured_img) ? $editSlider->featured_img : old('featured_img') }}"
                             style="margin-top:15px;max-height:100px;">
                        <a class="removeImgBtn btn btn-sm btn-danger " style="display: none">Remove</a>
                    </div>

                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="page_url">Page Url<span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-3">
                        <select name="page_type" id="page_type" class="form-control">
                            <option selected disabled>Select Page</option>
                            @isset($pageTypes)
                            @foreach($pageTypes as $type)
                            <option value="{{$type}}" {{(@$editSlider->page_type==$type || old('page_type')==$type) ?
                                'selected' : '' }}>{{str_replace('_','',$type)}}
                            </option>
                            @endforeach
                            @endisset
                        </select>
                    </div>

                    <div class="col-md-3">
                        <select name="url" id="url" class="form-control">
                            @if(@$editSlider->url)

                            @endif
                            <option selected disabled>Select List of Page</option>
                        </select>
                    </div>

                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="status">Status</label>
                    </div>
                    <div class=" col-md-9">
                        <div class="col-md-3">
                            <input type="radio" value="active" name="status" id="active" {{(@$editSlider->status=='active')
                            ? 'checked' : '' }} checked > &nbsp;<label
                                for="active">Active</label>
                        </div>
                        <div class="col-d-3">
                            <input type="radio" value="inactive" name="status" id="InActive" {{(@$editSlider->status=='inactive')
                            ? 'checked' : '' }}>&nbsp;<label
                                for="InActive">InActive</label>
                        </div>


                    </div>

                </div>


            </div>
            <div class="box-footer">

                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right">{{(@$editSlider) ? 'Updated': 'Created'}}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </form>
    </div>
</section>

<!--End Content Body-->
<!--main content-->

@endsection


@push('scripts')

<!--filemanager-->
<script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
<script>$('#lfm').filemanager('file');</script>
<!--standalone button-->
<!--Image Holder/hide and show image-->
<script src="{{asset('/build/pages/admin/shared/ImageHolder.js')}}"></script>
<script>
    let imageHolder = new ImageHolder('input[name="featured_img"]', '#holder', '.removeImgBtn');
</script>

<!--ck editor-->
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script>

    $('textarea').ckeditor();
</script>
<script>

    $('select[name="page_type"]').on('change', function () {
        var page_type = $(this).val();
        if (page_type) {
            var url = "{{(@$editSlider->url) ? $editSlider->url : old('url') }}";

            $('select[name="url"]').val(url);
            $.get(base_url + '/admin/page/getListOfPage/' + page_type, function (data) {
                var optionHtml = '';
                data.forEach(function (v) {
                    var url = base_url + '/' + page_type.toLowerCase() + '/' + v.title.replace(/\s/g, '_');
                    optionHtml += '<option value="' + url + '">' + v.title + '</option>';
                });

                $('select[name="url"]').html(optionHtml);
            })
        }

    }).trigger('change');
</script>
@endpush
