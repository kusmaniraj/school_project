@extends('layouts.admin')
@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endpush
@section('content')


<!-- Main content -->
<!--Content Header-->
<section class="content-header">
    <h1>
        {{@$title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{@$title}}</li>
    </ol>

</section>
<!--End of Content Header-->

<!--Content Body-->
<section class="content " id="slider">
    <!--  Alert Messages-->
    @include('admin.message.alertMessage')
    <!--    End of Alert Messages-->
    <!--  box -->
    <div class="box box-primary">
        <div class="box-header with-border">
                <h3 class="box-title"><strong> {{$title}} Page List</strong></h3>
                <a href="{{route('slider.show')}}" id="add_slider_btn" class="btn btn-primary pull-right">Add Slider </a>

            </div>
            <div class="box-body">
                <table id="slider_table" class="table" >
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @isset($listSliders)
                    @foreach($listSliders as $key=>$data)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$data->title}}</td>
                        <td>{{$data->status}}</td>
                        <td>
                            <a href="{{route('slider.delete',$data->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></a>
                            &nbsp;
                            <a href="{{route('slider.edit',$data->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>

                        </td>
                    </tr>
                    @endforeach
                    @endisset

                    </tbody>


                </table>
            </div>
    </div>

</section>

<!--End Content Body-->
<!--main content-->

@endsection
@push('scripts')

<!--datatable js-->
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>


@endpush
