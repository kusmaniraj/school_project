@extends('layouts.admin')
@push('styles')
<link rel="stylesheet" href="https://static.jstree.com/3.3.8/assets/dist/themes/default/style.min.css">
@endpush
@section('content')




<!-- Main content -->
<!--Content Header-->
<section class="content-header">
    <h1>
        {{@$title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{@$title}}</li>
    </ol>

</section>
<!--End of Content Header-->
<section class="content " id="page_menu">

    <!--  box -->
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title"><strong>Page Menu List</strong></h3>
                        </div>

                        <hr>
                        <div class="row" id="jstree"></div>
                        <hr>
                        <div class="row">
                            <button class="btn btn-success" id="saveTreeBtn">Save Changes</button>

                            <a href="{{route('menu.refresh')}}" class="btn btn-primary"><i class="fa fa-refresh"></i></a>
                        </div>


                    </div>
                    <div class="col-md-offset-2 col-md-6 " id="form_page">
                        <div class="box-header with-border">
                            <h3 class="box-title"><strong>Add Page Menu Form </strong>
                              </h3>
                            <a href="#" id="add_menu_btn" class="btn btn-primary pull-right">Add Menu</a>



                        </div>
                        <form id="menu_form">
                            <input type="hidden" name="id">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label for="title">Menu Title <span
                                                class="required text-danger">*</span></label>

                                        <input class="form-control" type="text" value="{{old('title')}}" name="title" readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label for="url">Menu Url <span class="required text-danger">*</span></label>
                                        <input type="text" class="form-control" id="url"
                                               placeholder="Enter Menu Url"
                                               name="url" value="{{old('url')}}" readonly>
                                    </div>
                                </div>






                                <div class="col-md-12">


                                    <div class="form-group ">
                                        <label for="featured_img">Featured Image</label>
                                        <span class="input-group-btn">
                                                 <a id="lfm" data-input="thumbnail" data-preview="holder"
                                                    class="btn btn-info">
                                                     <i class="fa fa-picture-o"></i> Choose Featured Image
                                                 </a>
                                               </span>
                                        <input id="thumbnail" class="form-control" type="hidden" name="featured_img">
                                        <img id="holder" style="margin-top:15px;max-height:100px;">
                                        <a class="removeImgBtn btn btn-sm btn-danger hidden">Remove</a>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label for="status">Status</label>
                                        <br>
                                        <input type="radio" id="statusActive" value="active" checked name="status"> &nbsp;<label
                                            for="statusActive">Active</label>
                                        <input id="statusInActive" type="radio" value="inactive" name="status">&nbsp;<label
                                            for="statusInActive">InActive</label>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <button type="submit" class="btn btn-primary">Created</button>



                                    </div>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>


</section>
<!--main content-->

<!--main content-->

@endsection

@push('scripts')

<!--filemanager-->
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<!--standalone button-->
<script>
    $('#lfm').filemanager('file');
</script>
<script src="https://static.jstree.com/3.3.8/assets/dist/jstree.min.js"></script>
<script src="{{asset('build/pages/admin/shared/shared.js')}}"></script>
<script src="{{asset('build/pages/admin/menu.js')}}"></script>
@endpush

