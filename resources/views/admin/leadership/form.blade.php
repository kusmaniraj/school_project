@extends('layouts.admin')

@push('styles')

@endpush
@section('content')


<!-- Main content -->
<!--Content Header-->
<section class="content-header">
    <h1>
        {{@$title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="{{route('leadership.index')}}">{{@$title}}</a></li>
        <li class="active">{{(@$editLeadership) ? 'Edit': 'Add'}} Form</li>
    </ol>

</section>
<!--End of Content Header-->

<!--Content Body-->
<section class="content " id="leadership-form">
    <!--  Alert Messages-->
    @include('admin.message.alertMessage')
    <!--    End of Alert Messages-->
    <!--  box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><strong> Leadership Form</strong></h3>
        </div>
        <div class="box-body">
            <form id="leadership_form" method="post"
                  action="{{(@$editLeadership->id) ? route('leadership.update',@$editLeadership->id) : route('leadership.store')}}">
                @csrf
                <input type="hidden" name="id" value="{{@$editLeadership->id}}">

                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="name">Name <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="name" class="form-control"
                               value="{{(@$editLeadership->name) ? $editLeadership->name : old('name')  }}">
                    </div>

                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="position">Position <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-4">
                        <select name="position" id="position" class="form-control">
                            <option value="" selected disabled>Select Position</option>

                            @if(@$editLeadership->position)
                            <option value="{{$editLeadership->position}}" selected >{{$editLeadership->position}}</option>
                            @else
                            @isset($positionArray)
                            @foreach($positionArray as $position)
                            <option value="{{$position}}" {{old('position')==$position ? 'selected' : ''}}>{{$position}}</option>
                            @endforeach
                            @endisset
                            @endif

                        </select>
                    </div>

                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Information <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-9">
                        <textarea name="information" id="" cols="100" rows="10">{{(@$editLeadership->information) ? $editLeadership->information : old('information')}}</textarea>
                    </div>

                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Profile Image <span class="required text-danger">*</span></label>
                    </div>
                    <div class="col-md-9">
                         <span class="input-group-btn">
                                                 <a id="lfm" data-input="thumbnail" data-preview="holder"
                                                    class="btn btn-info">
                                                     <i class="fa fa-picture-o"></i> Choose Profile Image
                                                 </a>
                                               </span>
                        <input id="thumbnail" class="form-control" type="hidden" name="profile_img"
                               value="{{(@$editLeadership->profile_img) ? $editLeadership->profile_img : old('profile_img')}}">
                        <img id="holder" style="margin-top:15px;max-height:100px;"
                             src="{{(@$editLeadership->profile_img) ? $editLeadership->profile_img : old('profile_img')}}">
                        <a class="removeImgBtn btn btn-sm btn-danger " style="display: none">Remove</a>


                    </div>

                </div>


                <div class="box-footer">

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right">{{(@$editLeadership) ? 'Updated':
                            'Created'}}
                        </button>
                        <a href="{{route('leadership.index')}}" class="btn btn-default">Close</a>
                    </div>

                </div>

            </form>
        </div>


    </div>
</section>

<!--End Content Body-->
<!--main content-->

@endsection


@push('scripts')

<!--filemanager-->
<script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
<!--standalone button-->
<script>
    $('#lfm').filemanager('file');
</script>
<!--Image Holder/hide and show image-->
<script src="{{asset('/build/pages/admin/shared/ImageHolder.js')}}"></script>

<script>
    let imageHolder = new ImageHolder('input[name="profile_img"]', '#holder', '.removeImgBtn');
</script>

<!--ck editor-->
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>


@endpush
