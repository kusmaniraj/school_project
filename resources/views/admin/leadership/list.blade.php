@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush
@section('content')


<!-- Main content -->
<!--Content Header-->
<section class="content-header">
    <h1>
        {{@$title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">{{@$title}} List</li>
    </ol>

</section>
<!--End of Content Header-->
<section class="content " id="page_leadership">
    <!--  Alert Messages-->
    @include('admin.message.alertMessage')
    <!--    End of Alert Messages-->
    <!--  box -->
    <div class="box box-primary">


        <div class="box-body">


            <div class="box-header with-border">
                <h3 class="box-title"><strong> Leaderships List</strong></h3>
                <a href="{{route('leadership.show')}}" id="add_leadership_btn" class="btn btn-primary pull-right">Add  Leadership </a>
            </div>
            <div class="box-body">
              <table id="leadership_table" class="table">
                  <thead>
                  <tr>
                      <th>SN</th>
                      <th>Name</th>
                      <th>Position</th>
                      <th>Information</th>
                      <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @isset($listLeaderships)
                  @foreach($listLeaderships as $key=>$leadership)
                  <tr>
                      <td>{{$key+1}}</td>
                      <td>{{$leadership->name}}</td>
                      <td>{{$leadership->position}}</td>
                      <td>{{$leadership->information}}</td>



                      <td>
                          <a href="{{route('leadership.delete',$leadership->id)}}"
                             class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></a>
                          &nbsp;
                          <a href="{{route('leadership.edit',$leadership->id)}}" class="btn btn-primary btn-xs"><i
                                  class="fa fa-edit"></i></a>

                      </td>

                  </tr>
                  @endforeach
                  @endisset
                  </tbody>


              </table>
            </div>


        </div>
     </div>


</section>

<!--main content-->

@endsection
@push('scripts')

<!--datatable js-->
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
    $('#leadership_table').dataTable();
</script>

@endpush
