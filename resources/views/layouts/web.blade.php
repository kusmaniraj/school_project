<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{config('setting.company_name')}} | @isset($title){{$title}}@endisset</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('Web/libraries/bootstrap/css/bootstrap.min.css')}}">
    <!--    Font awesome-->
    <link rel="stylesheet" href="{{asset('Web/libraries/font-awesome/css/font-awesome.css')}}">


    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{asset('Web/build/css/styles.css')}}">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!--    other styles-->
    @stack('styles')
    <script>var base_url='{{url("/")}}';</script>
</head>
<body class=" has-hero">
<!-- ******HEADER****** -->
@include('web.includes.header')
<!-- ******HEADER****** -->
<!--******MAIN CONTENT******-->
@yield('content')
<!--***END OF MAIN CONTENT**-->

<!-- ******FOOTER****** -->
@include('web.includes.footer')
<!--*******END OF FOOTER****-->



<!-- Javascript -->
<script type="text/javascript" src="{{asset('Web/libraries/jquery-3.1.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Web/libraries/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Web/libraries/bootstrap-hover-dropdown.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Web/libraries/back-to-top.js')}}"></script>
<script type="text/javascript" src="{{asset('Web/libraries/jquery-scrollTo/jquery.scrollTo.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Web/libraries/jquery-match-height/dist/jquery.matchHeight.js')}}"></script>
<script type="text/javascript" src="{{asset('Web/build//js/main.js')}}"></script>


@stack('scripts')

</body>
</html>
