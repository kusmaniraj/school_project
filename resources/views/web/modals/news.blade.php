<!-- News Modal 3-->
<div id="news-modal" class="news-modal modal modal-fullscreen" role="dialog" aria-labelledby="newsModal3Label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="modalLabel"></h4>

                <div class="meta text-center" id="modalDate"></div>
            </div><!--//modal-header-->
            <div class="modal-body">
                <div class="post">
                    <p><img class="img-responsive" src="" alt=""/></p>
                    <div id="modalDescription"></div>
                </div>
            </div><!--//modal-body-->
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary center-block" data-dismiss="modal">Close</button>
            </div>
        </div><!--//modal-content-->
    </div>
</div><!--//modal-->
