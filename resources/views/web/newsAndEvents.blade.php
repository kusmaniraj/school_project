@extends('layouts.web')
@push('styles')

@endpush

@section('content')
<!--******BANNER AND BREADCRUMB******-->
@include('web.includes.banner_breadcrumb')
<!--***END BANNER AND BREADCRUMB**-->

<div class="main-cols-wrapper">
    <section class="events-section">
        <div class="container">
            <h2 class="section-title">Events</h2>
            <div class="row">
                @isset($listContentOfEvent)
                @forelse($listContentOfEvent as $content)
                <div class="item col-xs-12 col-sm-4">
                    <div class="item-inner">
                        <div class="time">
                            <div class="time-inner">
                                <div class="date">26</div>
                                <div class="month">Jan</div>
                            </div>
                        </div><!--//time-->
                        <div class="details">
                            <h4 class="event-title">{{$content->title}}</h4>
                            <div class="meta">
                                <ul class="list-inline meta-list">
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i>{{$content->start_time}} - {{$content->end_time}}</li>
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i>{{$content->address}}</li>
                                </ul>
                            </div><!--//meta-->
                            <div class="details">
                                {!!str_limit($content->description,150,'....')!!}
                            </div><!--//details-->
                            <div class="meta">
                            </div><!--//meta-->
                        </div><!--//details-->
                    </div><!--//item-inner-->
                </div><!--//item-->
                @empty
                <p>No Content Found..</p>
                @endforelse
                @endisset



            </div><!--//row-->
            <div class="action text-center">
                <a class="btn btn-ghost-alt" href="calendar.html">View All<i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div>
        </div><!--//container-->
    </section><!--//events-section-->

    <section id="news-section" class="news-section section">
        <div class="container">
            <h2 class="section-title">News</h2>
            <div class="row">
                @isset($listContentOfNews)
                @forelse($listContentOfNews as $content)
                <div class="item col-xs-12 col-sm-4">
                    <div class="item-inner">
                        <div class="thumb-holder">
                            <img class="img-responsive" src="{{$content->featured_img}}" alt="{{$content->title}}">
                        </div><!--//thumb-holder-->
                        <div class="content-holder">
                            <h4 class="news-title"><a href="#">{{$content->title}}</a></h4>
                            <div class="intro">
                               {!!str_limit($content->description,200,'....')!!}
                            </div><!--//intro-->
                        </div><!--//content-holder-->
                        <a class="link" href="#" data-toggle="modal" data-target="#news-modal-1"></a>
                    </div><!--//item-inner-->
                </div><!--//item-->
                @empty
                <p>No Content Found..</p>
                @endforelse
                @endisset





            </div><!--//row-->
            <div class="action text-center">
                <a class="btn btn-ghost" href="#">Load More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div>
        </div><!--//container-->
    </section><!--//section-->
</div><!--//main-cols-wrapper-->

@endsection

@push('scripts')

@endpush
