@extends('layouts.web')
@push('styles')

@endpush

@section('content')
<!--******BANNER AND BREADCRUMB******-->
@include('web.includes.banner_breadcrumb')
<!--***BANNER AND BREADCRUMB**-->

<div class="main-cols-wrapper">
    <div class="container">
        <div class="row">
            <!--            Academic List Block-->
            <section class="col-main col-xs-12 col-md-12">
                @isset($listContentOfAcademic)
                @forelse($listContentOfAcademic as $content)
                <div class="block">
                    <h3 class="block-title">{{$content->title}}</h3>

                    <p>{!!substr( $content->description,0,600)!!}</p>
                    <br>
                    <a href="{{route('web.academic.detail',$content->title)}}" class="btn btn-ghost-alt">Read More</a>

                </div><!--//block-->
                @empty
                <p>No Content Found..</p>
                @endforelse

                @endisset




            </section>
            <!--            End of about Us Block-->



        </div><!--//row-->
    </div><!--//container-->
</div><!--//main-cols-wrapper-->

@endsection

@push('scripts')

@endpush
