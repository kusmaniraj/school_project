@extends('layouts.web')
@push('styles')

@endpush

@section('content')
    <section class="page-hero">
        <div class="hero-page-title container">
            <h1>Contact</h1>
        </div>
        <div class="mask"></div>
    </section>
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><i class="fa fa-home" aria-hidden="true"></i> <a href="{{url('/')}}">Home</a></li>
                <li class="active">Contact</li>
            </ol>
        </div><!--//container-->
    </div><!--//breadcrumb-container-->

    <div class="main-cols-wrapper">
        <div class="container">
            {{--alert Message--}}
            @include('web.alertMessage')
            {{--End of alert Message--}}
            <div id="contact-block" class="block contact-block">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="map-box">
                            <h3 class="map-title text-center">How to find us</h3>

                            <div class="gmap-wrapper">
                                <!--//You need to embed your own google map below-->
                                <!--//Ref: https://support.google.com/maps/answer/144361?co=GENIE.Platform%3DDesktop&hl=en -->
                                <iframe
                                    src="{{@$setting->google_map_iframe}}}"
                                    width="600" height="390" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div><!--//gmap-wrapper-->
                        </div><!--//map-->

                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-box">
                            <h3 class="text-center form-title">Contact Form</h3>

                            <form id="contact-form" class="contact-form form" method="post"
                                  action="{{route('web.sendMessage')}}">


                                @csrf
                                <div class="row text-center">
                                    <div class="contact-form-inner">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                                <label class="sr-only" for="cname">Your name</label>
                                                <input type="text" class="form-control" id="cname" name="name"
                                                       placeholder="Your name">
                                                @if ($errors->has('name'))
                                                    <p class="help-block text-center" role="alert">
                                                    <span>{{ $errors->first('name') }}</span>
                                                </p>
                                                @endif
                                            </div>
                                            <div
                                                class="col-md-6 col-sm-6 col-xs-12 form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                                                <label class="sr-only" for="cemail">Email address</label>
                                                <input type="email" class="form-control" id="cemail" name="email"
                                                       placeholder="Your email address">

                                                @if ($errors->has('email'))
                                                    <p class="help-block text-center" role="alert">
                                                    <span>{{ $errors->first('email') }}</span>
                                                </p>
                                                @endif
                                            </div>
                                            <div
                                                class="col-md-12 col-sm-12 col-xs-12 form-group  {{ $errors->has('subject') ? 'has-error' : '' }}">
                                                <label class="sr-only" for="subject">Subject</label>
                                                <input type="text" class="form-control" id="subject" name="subject"
                                                       placeholder="Your subject">
                                                @if ($errors->has('subject'))
                                                    <p class="help-block text-center" role="alert">
                                                    <span>{{ $errors->first('subject') }}</span>
                                                </p>
                                                @endif
                                            </div>
                                            <div
                                                class="col-md-12 col-sm-12 col-xs-12 form-group  {{ $errors->has('message') ? 'has-error' : '' }}">
                                                <label class="sr-only" for="cmessage">Your message</label>
                                        <textarea class="form-control" id="cmessage" name="message"
                                                  placeholder="Enter your message" rows="8"></textarea>
                                                @if ($errors->has('message'))
                                                    <p class="help-block text-center" role="alert">
                                                    <span>{{ $errors->first('message') }}</span>
                                                </p>
                                                @endif
                                            </div>
                                            <div
                                                class="col-md-12 form-group  {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                                                {!! NoCaptcha::display() !!}
                                                @if ($errors->has('g-recaptcha-response'))
                                                    <p class="help-block text-center" role="alert">
                                                    <span>{{ $errors->first('g-recaptcha-response') }}</span>
                                                </p>
                                                @endif
                                            </div>


                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                <button type="submit" class="btn btn-block btn-cta btn-primary">Send
                                                    Message
                                                </button>
                                            </div>
                                        </div><!--//row-->
                                    </div>
                                </div><!--//row-->
                            </form><!--//contact-form-->
                        </div><!--//form-box-->
                    </div>
                </div>


            </div><!--//block-->
        </div><!--//container-->
    </div><!--//main-cols-wrapper-->
    {!! NoCaptcha::renderJs() !!}
@endsection

@push('scripts')


@endpush
