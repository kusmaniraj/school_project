<footer class="footer">
    <div class="container">

        <div class="footer-col col-xs-12 col-md-12">
            <div class="footer-links row">
                <div class="sub-col col-xs-7 col-sm-3">
                    <h4 class="col-title">Contact Us</h4>

                    <div class="contact-details">
                        <div class="address">
                            {{config('setting.company_name')}}<br>{{config('setting.address')}}
                        </div><!--//address-->
                        <div class="contact">
                            <div class="item">T: <a href="#">{{config('setting.phone_number')}}</a></div>
                            <div class="item">E: <a href="#">{{config('setting.email')}}</a></div>
                        </div><!--//contact-->
                    </div>
                </div><!--//contact us-->
                <div class="sub-col col-xs-5 col-sm-3">
                    <h4 class="col-title">About Us</h4>
                    <ul class="footer-links list-unstyled">
                        @isset($listOfAboutUs)
                        @foreach($listOfAboutUs as $content)
                            <li class="link-item"><a href="{{url('about-us')}}">{{$content->title}}</a></li>
                        @endforeach
                        @endisset


                    </ul>
                </div><!--//about us-->
                <div class="sub-col col-xs-7 col-sm-3">
                    <h4 class="col-title">Social Links</h4>
                    <ul class="social-media list-inline">
                        <li><a href="{{config('setting.twitter_url')}}"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="{{config('setting.facebook_url')}}"><i class="fa fa-facebook"
                                                                            aria-hidden="true"></i></a></li>
                        <li><a href="{{config('setting.google_plus_url')}}"><i class="fa fa-google-plus"
                                                                               aria-hidden="true"></i></a></li>

                    </ul>
                </div><!--//social links-->
                <div class="sub-col col-xs-5 col-sm-3">
                    <h4 class="col-title">Useful Links</h4>
                    <ul class="footer-links list-unstyled">
                        @isset($bottomMenuPages)
                        @foreach($bottomMenuPages as $page)
                            <li class="link-item"><a href="{{$page->url}}}">{{$page->title}}</a></li>
                        @endforeach
                        @endisset


                    </ul>
                </div><!--//useful links-->


            </div><!--//row-->


        </div><!--//footer-col-->
        <div class="col-md-12 text-center   ">
            <small class="copyright">{{config('setting.company_name')}} Copyright @ <a href="{{url('/')}}"
                                                                                       target="_blank">{{date('Y',strtotime(config('setting.company_start_date')))}}</a>
            </small>
        </div>
    </div><!--//row-->


    </div>
    </div><!--//container-->
</footer><!--//footer-->
