<section class="page-hero"  style="background: url('{{asset(@$page->featured_img)}}') no-repeat center center;">
    <div class="hero-page-title container">
        <h1>{{@$title}}</h1>
    </div>
    <div class="mask"></div>
</section>
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li><i class="fa fa-home" aria-hidden="true"></i> <a href="{{url('/')}}">Home</a></li>
            @isset($parent)
            <li><a href="{{@$parent['url']}}">{{@$parent['title']}}</a></li>
            @endisset

            <li class="active">{{@$title}}</li>
        </ol>
    </div><!--//container-->
</div><!--//breadcrumb-container-->
