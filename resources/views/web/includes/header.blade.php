<header id="header" class="header">
    <div class="top-bar">

        <nav class="main-nav" role="navigation">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button><!--//nav-toggle-->
            </div><!--//navbar-header-->
            <div id="navbar-collapse" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">

                    @isset($topMenuPages)
                    @foreach($topMenuPages as $page)
                    @if(count($page->children) <=0)
                    <li class="{{$page->title==@$title ? 'active':''}} nav-item"><a href="{{$page->url}}">{{$page->title}}</a></li>
                    @else
                    <li class="nav-item dropdown">
                        <a class="dropdown-toggle nav-link" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">{{$page->title}} <i class="fa fa-angle-down"></i></a>
                        <div class="dropdown-menu">
                            @foreach($page->children as $child)
                            <a class="{{$page->title==@$title ? 'active':''}} dropdown-item" href="{{$child->url}}">{{$child->title}}</a>
                            @endforeach

                        </div>
                    </li><!--//dropdown-->
                    @endif
                    @endforeach
                    @endisset




                </ul><!--//nav-->
            </div><!--//navabr-collapse-->
        </nav><!--//main-nav-->


    </div><!--//top-bar-->
    <div class="branding">
        <div class="container">
            <h1 class="logo">
                <a href="{{url('/')}}"><img src="{{config('setting.logo_img')}}" alt=""></a>
            </h1><!--//logo-->
            <h2 class="tagline">{{config('setting.company_slogan')}}</h2>
        </div><!--//container-->
    </div><!--//branding-->
</header><!--//header-->
