@extends('layouts.web')
@push('styles')
<!--Flex slider Css-->
<link rel="stylesheet" href="{{asset('Web/libraries/flexslider/flexslider.css')}}">
@endpush

@section('content')
<section class="promo-section section section-on-bg">
    <div class="hero-slider-wrapper">
        <div class="flexslider hero-slider">
            <ul class="slides">
                @isset($sliders)
                @foreach($sliders as $i=>$slider)
                <li class="slide slide-3"
                    style="background: #212121 url('{{$slider->featured_img}}') no-repeat 50% 50%;">
                    <div class="container">
                        <div class="slide-box">
                            <div class="slide-box-inner">
                                <div class="text">{!!$slider->description!!}
                                </div>
                                <div class="more-link">
                                    <a class="btn btn-ghost" href="{{$slider->url}}">Read More</a>
                                </div>
                            </div><!--//slide-box-inner-->
                        </div><!--//slide-box-->
                    </div><!--//container-->
                </li>
                @endforeach
                @endisset

            </ul>
        </div>
    </div><!--//hero-slider-wrapper-->

    <div class="hero-overlay">
        <div class="container-fluid">

            <div class="overlay-upper">
                <div class="container">
                    <div class="contact-info pull-left">
                        <div class="item">Tel: <a href="tel:{{@$setting->phone_number}}">{{@$setting->phone_number}}</a></div>
                        <div class="item">Email: <a href="mailto:{{@$setting->email}}">{{@$setting->email}}</a>
                        </div>
                    </div><!--//contact-info-->
                    <ul class="social-media list-inline pull-right">
                        <li><a href="{{@$setting->twitter_url}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="{{@$setting->facebook_url}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="{{@$setting->google_plus_url}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="{{@$setting->skype_url}}"><i class="fa fa-skype" aria-hidden="true"></i></a></li>

                    </ul>
                </div><!--//container-->
            </div><!--//overlay-upper-->
            <div class="overlay-lower">
                <div class="container">
                    <div class="links">
                        <div class="link"><a href="about.html" title="Our School"><i class="fa fa-university link-icon"
                                                                                     aria-hidden="true"></i><span
                                    class="link-text">Our School</span></a></div>
                        <div class="link"><a href="admissions.html" title="Admissions"><i
                                    class="fa fa-graduation-cap link-icon" aria-hidden="true"></i><span
                                    class="link-text">Admissions</span></a></div>
                        <div class="link"><a href="{{route('web.newAndEvent')}}" title="News &amp; Events"><i
                                    class="fa fa-newspaper-o link-icon" aria-hidden="true"></i><span class="link-text">News &amp; Events</span></a>
                        </div>
                        <div class="link"><a href="docs.html" title="Key Info"><i class="fa fa-info-circle link-icon"
                                                                                  aria-hidden="true"></i><span
                                    class="link-text">Key Info</span></a></div>
                    </div>
                </div><!--//container-->
            </div><!--//overlay-lower-->
        </div>
    </div><!--//hero-overlay-->

    <div class="hero-badge">
        <div class="badge-content">
            <div class="script">Open Day</div>
            <div>2017</div>
            <a href="admissions.html" class="link-mask"></a>
        </div><!--//bagde-content-->
    </div><!--//hero-badge-->


</section><!--//promo-section-->
<section class="home-cols-wrapper">
    <div class="container">
        <!--       Welcome and Award block-->
        <div class="row">
            <div class="col-main col-xs-12 col-md-8">
                <div class="welcome-block block">
                    <div class="content">
                        <h3 class="block-title">Welcome from the Head Master</h3>

                        <div class="intro">
                            <p>{!!@$headmasterInfo->information!!}</p>

                            <div class="source">
                                <div class="name script">{{@$headmasterInfo->name}}</div>
                                <div class="title">{{@$headmasterInfo->position}}</div>
                            </div><!--//source-->
                        </div><!--//intro-->
                    </div><!--//content-->
                    <div class="figure">
                        <img src="{{asset('Web/images/headmaster.jpg')}}" alt="">
                    </div><!--//figure-->
                </div><!--//welcome-block-->
            </div>

            <div class="col-side col-xs-12 col-md-4">
                <div class="shortcuts-block block">
                    <div class="item bg-accent">
                        <i class="fa fa-download" aria-hidden="true"></i>
                        <span class="text"><a href="#">Download Prospectus</a></span>
                    </div><!--//item-->
                    <div class="item bg-dark">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                        <span class="text"><a href="#">Open Day 2017</a></span>
                    </div><!--//item-->
                    <div class="item bg-primary">
                        <i class="fa fa-binoculars" aria-hidden="true"></i>
                        <span class="text"><a href="#">Virtual Tour</a></span>
                    </div><!--//item-->
                    <div class="item bg-secondary">
                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                        <span class="text"><a href="#">Photo Gallery</a></span>
                    </div><!--//item-->
                </div><!--//shortcuts-block-->
            </div><!--//col-side-->
        </div>
        <!--        End of Welcome and Award Block-->

        <!--      News and Events-->
        <div class="row">
            <div class="col-main col-xs-12 col-md-8">
                <div class="news-block block">
                    <h3 class="block-title">Latest News</h3>

                    <div class="news-items">
                        @isset($news)
                        @forelse ($news as $data)
                        <div class="item item-1">
                            <div class="thumb-holder"
                                 style="background: url('{{$data->featured_img}}') no-repeat center center;">

                            </div><!--//thumb-holder-->
                            <div class="content-holder">
                                <h4 class="news-title"><a href="#" data-id="{{$data->id}}" class="modalNewsView">{{$data->title}}</a>
                                </h4>

                                <div class="intro">
                                    {!!str_limit($data->description, '200')!!}
                                </div>
                                <a class="btn btn-ghost modalNewsView" href="#" data-id="{{$data->id}}" >Read
                                    more<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        @empty
                        <p>No News....</p>
                        @endforelse
                        @endisset
                    </div>
                </div><!--//news-block-->
            </div>
            <div class="col-side col-xs-12 col-md-4">
                <div class="events-block block">
                    <h3 class="block-title">Upcoming Events</h3>

                    <div class="events-items">
                        @isset($events)
                        @forelse ($events as $data)
                        <div class="item">
                            <div class="time">
                                <div class="time-inner">
                                    <div class="date">26</div>
                                    <div class="month">Jan</div>
                                </div>
                            </div><!--//time-->
                            <div class="details">
                                <h4 class="event-title">{{$data->title}}</h4>

                                <div class="intro">
                                    {!!str_limit($data->description,200)!!}
                                </div><!--//intro-->
                            </div><!--//details-->
                        </div><!--//item-->
                        @empty
                        <p>No Events....</p>
                        @endforelse
                        @endisset
                        <div class="action text-center">
                            <a class="btn btn-ghost-alt" href="#">View All<i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div><!--//events-items-->
            </div>


        </div><!--//row-->

        <!--        End of News and Events-->
    </div><!--//container-->
</section><!--//home-cols-wrapper-->
<section class="reviews-section">
    <div class="container text-center">
        <div id="reviews-carousel" class="reviews-carousel carousel slide" data-ride="carousel">
            <!--//wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                @isset($studentReviews)
                @foreach ($studentReviews as $key=>$data)
                <div class="item {{$key==0 ? 'active':''}}">
                    <h3 class="review-title"><i class="fa fa-quote-left"></i>{{$data->title}}<i
                            class="fa fa-quote-right hidden-xs"></i>
                    </h3>
                    <blockquote class="review center-block">
                        <p>{!! str_limit($data->description,200)!!}</p>
                    </blockquote><!--//review-->
                    <div class="source">
                        <div class="name">{{$data->name}}</div>
                        <div class="title"> Grade -{{$data->grade}}&nbsp;Student &nbsp;{{$data->academic}}</div>
                    </div><!--//source-->
                </div><!--//item-->
                @endforeach
                @endisset



            </div><!--//carousel-inner-->

            <!--//Indicators-->
            <ol class="carousel-indicators">
                @isset($studentReviews)
                @foreach ($studentReviews as $key=>$data)
                <li class=" {{$key==0 ? 'active':''}}" data-target="#reviews-carousel" data-slide-to="{{$key}}">
                    <img class="img-responsive" src="{{$data->profileImg}}" alt="{{$data->title}}">
                </li>
                @endforeach
                @endisset

            </ol><!--//carousel-indicators-->

        </div>

    </div><!--//container-->
</section><!--//reviews-section-->

<!--Modal -->
<!--News-->
@include('web.modals.news')
<!--End of News-->
<!--End of Modal-->

@endsection

@push('scripts')
<!--Flex Slider Js-->
<script type="text/javascript" src="{{asset('Web/libraries/flexslider/jquery.flexslider-min.js')}}"></script>
<script type="text/javascript" src="{{asset('Web/build/js/home.js')}}"></script>
<!-- modal view-->
<script>
    $('.modalNewsView').on('click',function(){
        var newsId=$(this).data('id');
        var $newModalSel=$('#news-modal');
        $newModalSel.modal('show');
        $.get(base_url+'/getRowPageInfo/'+newsId,function(data){
            $newModalSel.find('#modalLabel').text(data.title);
            $newModalSel.find('#modalDate').text(data.date);
            $newModalSel.find('img').attr('src',data.featured_img);
            $newModalSel.find('#modalDescription').html(data.description);
        })
    })
</script>
@endpush
