@extends('layouts.web')
@push('styles')

@endpush

@section('content')
    <!--******BANNER AND BREADCRUMB******-->
@include('web.includes.banner_breadcrumb')
    <!--***BANNER AND BREADCRUMB**-->

<div class="main-cols-wrapper">
    <div class="container">

        <div class="row">
            {{--List of Notices Block--}}
            <section id="notice-list-block" class="col-main col-xs-12 col-md-12  block">


                <div class="list row">
                    @isset($listContentOfNotice)
                    @forelse($listContentOfNotice as $content)
                        <div class="notice-item col-12 col-md-6">
                            <div class="inner">
                                <div class="notice-header">
                                    <h3 class="notice-title">{{$content->title}}</h3>

                                </div>
                                <div class="notice-content">
                                    <small><span></span><i class="fa fa-calendar"></i>&nbsp;&nbsp;{{$content->date}}</small>
                                    <div class="desc">
                                        <p>{!! $content->description !!}</p>
                                    </div><!--//desc-->
                                    <a href="{{route('web.notice.detail',$content->trimTitle)}}" class="btn btn-ghost-alt">Details</a>
                                </div><!--//notice-content-->

                            </div><!--//inner-->
                        </div>
                    @empty
                        <p>No Content Found..</p>
                    @endforelse

                    @endisset


                </div><!--//row-->
            </section>
            {{--End of List Notices--}}


        </div><!--//row-->
    </div><!--//container-->
</div><!--//main-cols-wrapper-->

@endsection

@push('scripts')

@endpush
