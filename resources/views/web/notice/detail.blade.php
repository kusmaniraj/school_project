@extends('layouts.web')
@push('styles')

@endpush

@section('content')
<!--******BANNER AND BREADCRUMB******-->
@include('web.includes.banner_breadcrumb')
<!--***BANNER AND BREADCRUMB**-->

<div class="main-cols-wrapper">
    <div class="container">
        <div class="row">
            <!--            About Us Block-->
            <section class="col-main col-xs-12 col-md-8">

                <div class="block">
                    <h3 class="block-title">{{$notice->title}}</h3>
                    <p>{!! $notice->description!!}</p>

                </div><!--//block-->




            </section>
            <!--            End of about Us Block-->

            <aside class="col-side col-xs-12 col-md-4">

                <div class="col-side-inner">

                    <div class="list-block">
                        <h2 class="block-title">Latest Notices</h2>
                        <div class="item">
                            @isset($listContentOfNotice)
                            @foreach($listContentOfNotice as $content)
                                <ul>
                                    <li>
                                        <a href="{{route('web.notice.detail',$content->title)}}">{{$content->title}}</a>
                                    </li>
                                </ul>

                            @endforeach

                            @endisset


                        </div>
                    </div>

                </div><!--//col-side-inner-->
            </aside><!--//col-side-->

        </div><!--//row-->
    </div><!--//container-->
</div><!--//main-cols-wrapper-->

@endsection

@push('scripts')

@endpush
