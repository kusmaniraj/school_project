

@if(Session::has('success'))
<div class="alert alert-success alert-status">
    <strong>Successfully !!</strong>&nbsp;&nbsp;{{Session::get('success')}}
</div>

@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-status">
    <strong>Error !!</strong>&nbsp;&nbsp;{{Session::get('error')}}
</div>

@endif



@push('scripts')
<script>
    window.setTimeout(function() {
        $('.alert-status').slideUp(800,function(){
            $(this).remove();
        });
    },2000)

</script>


@endpush
