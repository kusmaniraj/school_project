@extends('layouts.web')
@push('styles')

@endpush

@section('content')
<!--******BANNER AND BREADCRUMB******-->
@include('web.includes.banner_breadcrumb')
<!--***BANNER AND BREADCRUMB**-->

<div class="main-cols-wrapper">
    <div class="container">
        <div class="row">
            <!--            About Us Block-->
            <section class="col-main col-xs-12 col-md-8">
                @isset($listContentOfAdmission)
                @forelse($listContentOfAdmission as $content)
                <div class="block">
                    <h3 class="block-title">{{$content->title}}</h3>

                    <p>{!! $content->description!!}</p>

                </div><!--//block-->
                @empty
                <p>No Content Found..</p>
                @endforelse

                @endisset




            </section>
            <!--            End of about Us Block-->

            <aside class="col-side col-xs-12 col-md-4">
                <div class="col-side-inner">
                    <div class="video-block block">
                        <div class="video-thumb-holder">
                            <img class="video-thumb img-responsive" src="assets/images/sidebar/video-thumb-1.jpg"
                                 alt="">
                            <a href="#" class="play-trigger" data-toggle="modal" data-target="#modal-video"><img
                                    class="play-icon" src="assets/images/play-icon.svg" alt=""></a>

                            <div class="mask"></div>
                        </div><!--//video-thumb-holder-->
                        <div class="video-intro">
                            <h4 class="video-title"><a href="#" data-toggle="modal" data-target="#modal-video">A Day at
                                    Academy</a></h4>

                            <div class="video-desc">Video intro ipsum dolor sit amet consec adipiscing elit. Aenean
                                commodo ligula eget dolor.
                            </div>
                        </div><!--//video-intro-->
                    </div><!--//video-block-->
                    <div class="file-block block">
                        <div class="file-thumb-holder">
                            <img class="file-thumb img-responsive" src="assets/images/sidebar/file-thumb-1.jpg" alt="">
                            <a class="download-link" href="#"><img class="download-icon"
                                                                   src="assets/images/download-icon.svg" alt=""></a>

                            <div class="mask"></div>
                        </div><!--//file-thumb-holder-->
                        <div class="file-intro">
                            <h4 class="file-title"><a href="#">School Prospectus 2017</a></h4>
                        </div><!--//file-intro-->
                    </div><!--//file-block-->

                    <div class="cta-block block">
                        <div class="cta-button">
                            <a class="btn btn-secondary btn-block btn-cta" href="#">Admission Process</a>
                        </div><!--//cta-button-->
                        <div class="cta-button">
                            <a class="btn btn-secondary btn-block btn-cta" href="#">Admission Fee</a>
                        </div><!--//cta-button-->
                        <div class="cta-button">
                            <a class="btn btn-secondary btn-block btn-cta" href="#">Apply Now</a>
                        </div><!--//cta-button-->
                    </div>
                </div><!--//col-side-inner-->
            </aside><!--//col-side-->

        </div><!--//row-->
    </div><!--//container-->
</div><!--//main-cols-wrapper-->

@endsection

@push('scripts')

@endpush
