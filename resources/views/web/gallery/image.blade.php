@extends('layouts.web')
@push('styles')
<link rel="stylesheet" href="{{asset('Web/libraries/Gallery/css/blueimp-gallery.min.css')}}">
@endpush

@section('content')
<!--******BANNER AND BREADCRUMB******-->
@include('web.includes.banner_breadcrumb')
<!--***BANNER AND BREADCRUMB**-->

<div class="main-cols-wrapper">
    <div class="container">
        <div class="row">
            <!--           Album Block-->
            <section class="col-main col-xs-12 col-md-8 album-block block">
                <h3 class="album-title">
                    Album {{$albumInfo->albumName}}
                </h3>
                <div class="album-intro">
                  {!! $albumInfo->description !!}
                </div>
                <div id="school-album" class="row">
                    @isset($albumImages)
                    @forelse($albumImages as $i=>$image)
                        <div class="item col-xs-6 col-md-4" title="image-{{$i+1}}">
                            <a href="{{$image->image}}">
                                <img class="img-responsive" src="{{$image->image}}" alt="image-{{$i+1}}">
                            </a>
                        </div>
                    @empty
                        <p>No Content Found..</p>
                    @endforelse

                    @endisset



                </div>
                <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="close">×</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>
            </section>







            <!--            End of Album Block-->

            <aside class="col-side col-xs-12 col-md-4">
                <div class="col-side-inner">
                    <div class="video-block block">
                        <div class="video-thumb-holder">
                            <img class="video-thumb img-responsive" src="assets/images/sidebar/video-thumb-1.jpg"
                                 alt="">
                            <a href="#" class="play-trigger" data-toggle="modal" data-target="#modal-video"><img
                                    class="play-icon" src="assets/images/play-icon.svg" alt=""></a>

                            <div class="mask"></div>
                        </div><!--//video-thumb-holder-->
                        <div class="video-intro">
                            <h4 class="video-title"><a href="#" data-toggle="modal" data-target="#modal-video">A Day at
                                    Academy</a></h4>

                            <div class="video-desc">Video intro ipsum dolor sit amet consec adipiscing elit. Aenean
                                commodo ligula eget dolor.
                            </div>
                        </div><!--//video-intro-->
                    </div><!--//video-block-->
                    <div class="file-block block">
                        <div class="file-thumb-holder">
                            <img class="file-thumb img-responsive" src="assets/images/sidebar/file-thumb-1.jpg" alt="">
                            <a class="download-link" href="#"><img class="download-icon"
                                                                   src="assets/images/download-icon.svg" alt=""></a>

                            <div class="mask"></div>
                        </div><!--//file-thumb-holder-->
                        <div class="file-intro">
                            <h4 class="file-title"><a href="#">School Prospectus 2017</a></h4>
                        </div><!--//file-intro-->
                    </div><!--//file-block-->

                    <div class="cta-block block">
                        <div class="cta-button">
                            <a class="btn btn-secondary btn-block btn-cta" href="#">Admission Process</a>
                        </div><!--//cta-button-->
                        <div class="cta-button">
                            <a class="btn btn-secondary btn-block btn-cta" href="#">Admission Fee</a>
                        </div><!--//cta-button-->
                        <div class="cta-button">
                            <a class="btn btn-secondary btn-block btn-cta" href="#">Apply Now</a>
                        </div><!--//cta-button-->
                    </div>
                </div><!--//col-side-inner-->
            </aside><!--//col-side-->

        </div><!--//row-->
    </div><!--//container-->
</div><!--//main-cols-wrapper-->

@endsection

@push('scripts')
<script src="{{asset('Web/libraries/Gallery/js/jquery.blueimp-gallery.min.js')}}"></script>
<script>
    document.getElementById('school-album').onclick = function (event) {
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event},
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
    };
</script>
@endpush
