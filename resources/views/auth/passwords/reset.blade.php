@extends('layouts.auth')

@section('content')

<div class="auth-box">
    <div class="auth-logo">
        <a href="{{url('/')}}"><b>{{ config('app.name') }}</b></a>
    </div>
    <!-- /.auth-logo -->
    <div class="auth-box-body">
        <h4 class="auth-box-msg">Reset Password</h4>

        <form method="POST" action="{{ route('password.update') }}">

            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                <input type="email" class="form-control" placeholder="Email" value="{{old('email')}}" name="email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group has-feedback ">
                <input type="password" class="form-control" placeholder="Password Confirmation" name="password_confirmation">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>

            </div>

            <div class="row">

                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Password</button>
                </div>
                <!-- /.col -->
            </div>
        </form>




    </div>
    <!-- /.auth-box-body -->
</div>

@endsection
