<table width="100%" >
    <tr>
        <td colspan="2">
            <h1 style="font-size: 20px;">A new message from contact form!</h1>
        </td>
    </tr>
    <tr >
        <td width="100px"><b>From:</b></td>
        <td><p> {{ @$contactName }} <i>({{ @$contactEmail }})</i></p></td>
    </tr>
    <tr>
        <td width="100px"><b>Subject:</b></td>
        <td>{{ @$contactSubject }}</td>
    </tr>
    <tr>
        <td width="100px"><b>Message:</b></td>
       <td> <p>{{ @$contactMessage }}</p></td>
    </tr>

</table>
