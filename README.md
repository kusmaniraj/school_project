# School portfolio

## Requirements

* PHP >= 7.2


## Installation

Install dependencies

```bash
composer install
```

Copy the `.env` file and adjust settings if need be

```
cp .env.example .env
```

### Database settings

Update the configuration in `.env` file like so.

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=PROJECT
DB_USERNAME=<username>
DB_PASSWORD=<password>
```
### Mail Setting


````
MAIL_DRIVER=<driver like: smpt, pop3 etc>
MAIL_HOST=<host>
MAIL_PORT=<port>
MAIL_USERNAME=<username>
MAIL_PASSWORD=<password>
MAIL_ENCRYPTION=<encrypt if exists then null>
````


### Application key

Generate an application key with this command:

```bash
php artisan key:generate
```

## Run the server

```bash
php artisan serve
```

## Usage
The App is available at this url: `http://localhost:8000`

## Troubleshooting

Clear the config cache:

```bash
php artisan config:clear
php artisan config:cache
```
