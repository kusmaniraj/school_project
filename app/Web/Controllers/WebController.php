<?php

namespace App\Web\Controllers;

use App\Admin\Auth\Models\User;
use App\Admin\Setting\Repositories\SettingRepository;

use App\Web\Repositories\WebRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Session;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\HelperTrait;
use Infrastructure\Mail\CustomerMail;


class WebController extends Controller
{
    private $data;
    private $setting;
    private $web;

    use HelperTrait;

    public function __construct(SettingRepository $setting, WebRepository $web)
    {
//        load repositories

        $this->setting = $setting;
        $this->web = $web;
        $this->data['title'] = 'Home';
        $this->data['setting'] = $this->setting->getRow();

        $this->data['topMenuPages'] = $this->web->topMenuNav();
        $this->data['bottomMenuPages'] = $this->web->topMenuNav();
        $this->data['listOfAboutUs']=$this->web->getResultsContentOfPage('About_Us');


    }


    public function home()
    {

        $this->data['title'] = 'Home';
        $this->data['headmasterInfo'] = $this->web->getRowOfLeadershipByPosition('Head Master');
        $this->data['sliders'] = $this->web->getResultsOfSlider(4);
        $this->data['news'] = $this->web->getResultsContentOfPage('News', 3);
        $this->data['events'] = $this->web->getResultsContentOfPage('Event', 3);
        $this->data['studentReviews'] = $this->web->getStudentReviews(5);

        return view('web.home', $this->data);
    }

    public function newsAndEventsPage()
    {
        $title='News & Events';
        $this->data['title'] = $title;
        $this->data['listContentOfNews'] = $this->web->getResultsContentOfPage('News');
        $this->data['listContentOfEvent'] = $this->web->getResultsContentOfPage('Event');
        $this->data['page'] = $this->web->getRowMenu($title);

        return view('web.newsAndEvents', $this->data);
    }



    public function aboutUsPage()
    {
        $title = 'About Us';
        $this->data['title'] = $title;
        $this->data['page'] = $this->web->getRowMenu($title);
        $this->data['listContentOfAboutUs'] = $this->web->getResultsContentOfPage('About_Us');
        $this->data['leaderships'] = $this->web->getResultsOfLeadership();
        return view('web.aboutUs', $this->data);
    }


    public function contactPage()
    {
        $title='Contact';
        $this->data['title'] = $title;
        $this->data['page'] = $this->web->getRowMenu($title);
        return view('web.contact', $this->data);
    }
    public function sendCustomerMessage(Request $request){
        $rule=[
            'name'=>'required|max:50',
            'email'=>'required|email',
            'subject'=>'required|max:100',
//            'g-recaptcha-response' => 'required|captcha',
            'message'=>'required|max:200'
        ];
        $message=[
            'name.required'=>'*Please Input Name.*',
            'name.max'=>'*Name must not be greater than 50 character.*',
            'email.required'=>'*Please Input Email.*',
            'email.regex'=>'*Invalid Email.*',
            'subject.required'=>'*Please Input Subject.*',
            'subject.max'=>'*Subject must not be greater than 100 character.*',
            'message.required'=>'*Please Input Message.*',
            'message.max'=>'*Message must not be greater than 200 character.*',

        ];
        $request->validate($rule,$message);

        $result=$this->web->storeMessage($request->all());
        if($result){
            Mail::to(auth()->user())->send(new CustomerMail($request));
            Session::flash('success','*Send Message. *');
        }else{
            Session::flash('error','*Could not send Message. *');
        }
        return redirect()->back();


    }

    public function admissionPage()
    {
        $title = 'Admissions';
        $this->data['title'] = $title;
        $this->data['page'] = $this->web->getRowMenu('Admission');
        $this->data['listContentOfAdmission'] = $this->web->getResultsContentOfPage('Admission');
        return view('web.admission', $this->data);
    }
    public function albumPage()
    {
        $title = 'Albums';
        $this->data['title'] = $title;
        $this->data['page'] = $this->web->getRowMenu('Gallery');
        $this->data['albums'] = $this->web->getResultsOfAlbums();
        return view('web.gallery.album', $this->data);
    }
    public function albumImagesPage($albumName)
    {
        $parentNav=[
            'title'=>'Albums',
            'url'=>route('web.albums')
        ];
        $title = 'Images';


        $albumInfo=$this->web->getRowOfAlbumByName($albumName);
        if($albumInfo==null){
            abort(404);
        }
        $this->data['albumInfo']=$albumInfo;
        $this->data['albumImages'] = $this->web->getResultsOfAlbumImages($albumName);
        $this->data['title'] = $title;
        $this->data['parent'] = $parentNav;
        $this->data['page'] = $this->web->getRowMenu('Gallery');
        return view('web.gallery.image', $this->data);
    }
    public function academicPage(){
        $title='Academic';
        $this->data['title'] =$title;
        $this->data['listContentOfAcademic'] = $this->web->getResultsContentOfPage($title);
        $this->data['page'] = $this->web->getRowMenu($title);
        return view('web.academic.list', $this->data);
    }

    public function academicDetailPage($title){

        $parent=[
            'title'=>'Academic',
            'url'=>route('web.academic')
        ];
        $academicDetail=$this->web->getRowContentOfPageByTitle($parent['title'],$title);
        if($academicDetail==null){
            abort(404);
        }
        $this->data['academic']=$academicDetail;
        $this->data['listContentOfAcademic'] = $this->web->getResultsContentOfPage($parent['title']);
        $this->data['parent'] = $parent;
        $this->data['title'] =$title;
        $this->data['page'] = $this->web->getRowMenu($parent['title']);
        return view('web.academic.detail', $this->data);
    }


    public function noticePage(){
        $title='Notice';
        $this->data['title'] =$title;

        $listNotices= collect($this->web->getResultsContentOfPage($title))->map(function ($item) {
            $item->date = date('j F,Y', strtotime($item->date));
            $item->description= substr($item->description,0,250);
            $item->trimTitle=$this->stringReplace(' ','-',$item->title) ;
            return $item;
        });

        $this->data['listContentOfNotice'] = $listNotices;
        $this->data['page'] = $this->web->getRowMenu($title);
        return view('web.notice.list', $this->data);
    }
    public function noticeDetailPage($title){

        $parent=[
            'title'=>'Notice',
            'url'=>route('web.notice')
        ];
        $academicDetail=$this->web->getRowContentOfPageByTitle($parent['title'],$title);
        if($academicDetail==null){
            abort(404);
        }
        $this->data['notice']=$academicDetail;
        $this->data['listContentOfNotice'] = $this->web->getResultsContentOfPage($parent['title'],5);
        $this->data['parent'] = $parent;
        $this->data['title'] =$title;
        $this->data['page'] = $this->web->getRowMenu($parent['title']);
        return view('web.notice.detail', $this->data);
    }
}
