<?php
namespace App\Web\Repositories;

use App\Admin\Gallery\Models\Album;
use App\Admin\Gallery\Models\AlbumImage;
use App\Admin\Leadership\Models\Leadership;
use App\Admin\Menu\Models\Menu;
use App\Admin\Message\Models\Message;
use App\Admin\Page\Models\Page;
use App\Admin\Slider\Models\Slider;
use App\Admin\Student\Models\StudentReview;
use Carbon\Carbon;

class WebRepository
{


    public function __construct()
    {

    }

    public function getRowMenu($title)
    {
        return Menu::where(['title' => $title])->first();
    }

    public function topMenuNav()
    {


        $parentList = Menu::where(['status' => 'active', 'parent_id' => 0])->orderBy('position', 'asc')->get();
        foreach ($parentList as $key => $parent) {
            $childList = Menu::where(['status' => 'active', 'parent_id' => $parent->id])->orderBy('position', 'asc')->get();
            $parentList[$key]['children'] = $childList;

        }

        return $parentList;
    }

    //leadership
    public function getResultsOfLeadership()
    {
        return Leadership::all();
    }

    public function getRowOfLeadership($id)
    {
        return Leadership::findOrFail($id);
    }

    public function getRowOfLeadershipByPosition()
    {
        return Leadership::where(['Position' => 'Head Master'])->first();
    }

    //slider
    public function getResultsOfSlider($limit = null)
    {

        $query = Slider::where(['status' => 'active']);
        if ($limit) {
            $query->take($limit);
        }
        $query->orderBy('updated_at', 'desc');
        return $query->get();
    }


    //student Reviews
    public function getStudentReviews($limit = null)
    {
        $query = StudentReview::where(['status' => 'active']);
        if ($limit) {
            $query->take($limit);
        }
        $query->orderBy('updated_at', 'desc');
        return $query->get();
    }

//    pages

    public function getResultsContentOfPage($pageType, $limit = null)
    {
        $query = Page::where(['page_type' => $pageType, 'status' => 'active']);
        if ($limit) {
            $query->take($limit);
        }
        $query->orderBy('updated_at', 'desc');
        return $query->get();


    }

    public function getRowContentOfPage($id)
    {
        $pageInfo = Page::findOrFail($id);
        if ($pageInfo) {
            $pageInfo->date = date('j F,Y', strtotime($pageInfo->date));;
        }
        return $pageInfo;
    }
    public function getRowContentOfPageByTitle($pageType,$title)
    {
        $pageInfo = Page::where(['page_type' => $pageType, 'title' => $title])->first();
        if ($pageInfo) {
            $pageInfo->date = date('j F,Y', strtotime($pageInfo->date));;
        }
        return $pageInfo;
    }
    //gallery
    public function getResultsOfAlbums($limit=null){

        $query = Album::where(['status' => 'active']);
        if ($limit) {
            $query->take($limit);
        }
        $query->orderBy('updated_at', 'desc');
        return $query->get();
    }
    public function getRowOfAlbumByName($name){
        return Album::where(['albumName' => $name])->first();
    }
    public function getResultsOfAlbumImages($albumName,$limit=null){
        $albumName=str_replace('-',' ',$albumName);
        $album=$this->getRowOfAlbumByName($albumName);
        $query = AlbumImage::where(['albumId' => $album->id]);
        if ($limit) {
            $query->take($limit);
        }
        $query->orderBy('updated_at', 'desc');
        return $query->get();
    }

//    Message
    public function storeMessage($data)
    {
        return Message::create($data);
    }


}
