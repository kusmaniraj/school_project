<?php

namespace App\Admin\Admin\Controllers;


use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;
use Modules\Admin\Entities\User;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Http\Request;

use Auth;

class ProfileController extends Controller
{
    private $data;
    use AdminTrait;

    public function __construct()
    {
        $this->data['title'] = 'Profile';
//        $this->data['messages'] = $this->getCustomerMessages();


    }

    public function index()
    {
        $this->data['admin'] = Auth::user();
        return view('admin.profile')->with($this->data);
    }

    public function update(Request $request, $id)
    {


//        update profile Information Only
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id,' . $id,


        ]);
        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator);
        }
        if ($request->old_password == '' && $request->password == '' && $request->password_confirmation == '') {
            $result = User::find($id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'profile_img' => $request->profile_img,
                'gender' => $request->gender,

            ]);
            if ($result) {
                $request->session()->flash('success', 'Profile has been Updated');
            } else {
                $request->session()->flash('error', 'Cannot Updated Profile  ');
            }
            return redirect()->back();
        }


//change Password

        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|string|min:6|confirmed',

        ]);
        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator);
        }
        if (Hash::check($request->old_password, Auth::user()->password) == false) {
            $validator = ['old_password' => 'Old Password Not Match'];
            return redirect()->back()->withErrors($validator);
        } else {

            $result = User::find($id)->update([
                'password' => Hash::make($request->password),
            ]);
            if ($result) {
                $request->session()->flash('success', 'Changed the Password');
            } else {
                $request->session()->flash('error', 'Cannot Changed the Password  ');
            }
            return redirect()->back();
        }


    }
}
