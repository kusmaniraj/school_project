<?php

namespace App\Admin\Leadership\Models;

use Illuminate\Database\Eloquent\Model;

class Leadership extends Model
{
    protected $table='staffs';
    protected $fillable=['id','name','profile_img','status','position','information'];

}
