<?php
/**
 * Created by IntelliJ IDEA.
 * User: Niraj Kusma
 * Date: 12/8/2019
 * Time: 7:05 PM
 */
namespace App\Admin\Leadership\Repositories;


use App\Admin\Leadership\Models\Leadership;

class LeadershipRepository
{
    private $leadership;

    public function __construct(Leadership $leadership)
    {
        $this->leadership = $leadership;
    }

    /**
     * fetch row of data
     * @param $id
     * @return mixed
     */
    public function getRow($id)
    {
        return $this->leadership->findOrFail($id);
    }

    /**
     * fetch all results
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getResults()
    {
        return $this->leadership->all();
    }

    /**
     * insert a data
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->leadership->create($data);
    }

    /**
     * update row of data
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        return $this->leadership->findOrFail($id)->update($data);
    }

    /**
     * delete row of data
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->leadership->findOrFail($id)->delete();
    }

    public function checkPosition($position){
        if( $this->leadership->where(['position'=>$position])->count() > 0){
            return true;
        }else{
            return false;
        }
    }
}
