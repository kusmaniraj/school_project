<?php

namespace App\Admin\Leadership\Controllers;


use App\Admin\Leadership\Repositories\LeadershipRepository;
use Illuminate\Http\Request;

use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;
use Illuminate\Support\Facades\Session;
use Infrastructure\Http\Traits\DataArrayTrait;

class LeadershipController extends Controller
{
    use AdminTrait;
    use DataArrayTrait;
    protected $data;
    private $leadership;


    protected $rule = [
        'name' => 'required|max:100',
        'profile_img' => 'required',
        'position' => 'required',
        'information' => 'required'

    ];
    protected $message = [
        'name.required' => '*Please Input Leadership Name *.',
        'name.max' => '*Leadership Name must not be greater than 255 character *.',
        'information.required' =>'*Please Input Leadership Information *.',

        'profile_img.required' => '*Please Choose Leadership Profile *.',
        'position.required' => '*Please Select Leadership Position *.',


    ];

    public function __construct(LeadershipRepository $leadership)
    {
        //load repository
        $this->leadership = $leadership;
        $this->data['title'] = 'Leaderships';

    }

    /**
     * view list of Leaderships
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $this->data['listLeaderships'] = $this->leadership->getResults();
        return view('admin.leadership.list', $this->data);
    }

    /**
     * show leadership form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $this->data['positionArray'] = $this->positionArray();
        return view('admin.leadership.form', $this->data);
    }

    /**
     * create form data of leadership
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate($this->rule, $this->message);
        if($this->leadership->checkPosition($request->position)==true){
            Session::flash('error', 'Selected Position ('.$request->position.') is already taken.');
            return redirect()->back()->withInput();
        }

        $result = $this->leadership->create($request->all());

        $alertMessage = ' a leadership (' . $request->name . ' )';
        if ($result) {
            Session::flash('success', 'Created' . $alertMessage);
            return redirect()->route('leadership.index', $result->id);
        } else {
            Session::flash('error', ' Could not be create' . $alertMessage);
            return redirect()->back();
        }

    }

    /**
     * edit Leadership
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $this->data['editLeadership'] = $this->leadership->getRow($id);
        return view('admin.leadership.form', $this->data);
    }

    /**
     * Update form data of Leadership
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->rule, $this->message);
        $result = $this->leadership->update($id, $request->all());
        $alertMessage = ' a leadership (' . $request->name . ' )';
        if ($result) {
            Session::flash('success', 'Updated' . $alertMessage);

        } else {
            Session::flash('error', ' Could not be update' . $alertMessage);

        }
        return redirect()->back();
    }

    /**
     * delete leadership
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $leadershipInfo = $this->leadership->getRow($id);
        $result = $this->leadership->delete($id);
        $alertMessage = ' a leadership (' . $leadershipInfo->name . ' )';
        if ($result) {
            Session::flash('success', 'Deleted' . $alertMessage);
        } else {
            Session::flash('error', ' Could not delete' . $alertMessage);


        }
        return redirect()->back();

    }


}
