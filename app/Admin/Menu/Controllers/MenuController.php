<?php

namespace App\Admin\Menu\Controllers;


use App\Admin\Menu\Models\Menu;
use App\Admin\Page\Models\Page;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;
use Validator;
use DB;

class MenuController extends Controller
{
    protected $data;
    use AdminTrait;

    protected $rule = [
        'title' => 'required',
        'url' => 'required',

    ];


    public function __construct()
    {

        $this->data['title'] = 'Menu';


    }

    public function index()
    {


        return view('admin.menu', $this->data);
    }

    public function getMenus()
    {
        return $this->jstreeFormat();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rule);
        if ($validator->fails()) {
            return response()->json(['validation_errors' => $validator->errors()]);
        }
        $formData = $request->all();
        $formData['position'] = 0;
        $result = Menu::create($formData);
        if ($result) {
            return ['success' => 'Created the  Menu'];
        } else {
            return ['error' => ' Cannot Created the  Menu'];
        }
    }

    public function edit($id)
    {
        return Menu::find($id);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rule);
        if ($validator->fails()) {
            return response()->json(['validation_errors' => $validator->errors()]);
        }
        $result = Menu::find($id)->update($request->all());
        if ($result) {
            return ['success' => 'Update the  Menu'];
        } else {
            return ['error' => ' Cannot update the  Menu'];
        }
    }

    public function refreshMenuPages()
    {

        $menuPages = $this->navMenus();

        if ($menuPages) {
            foreach ($menuPages as $i => $page) {

                $prevParentInfo = DB::table('menus')->where(['title' => $page['title'], 'parent_id' => 0])->first();
                if ($prevParentInfo || $prevParentInfo != null) {
                    $parentId = $prevParentInfo->id;

                } else {
                    $storeData = [
                        'title' => $page['title'],
                        'url' => $page['url'],
                        'parent_id' => 0,
                        'position' => $i
                    ];
                    $parentId = DB::table('menus')->insertGetId($storeData);
                }


                if ($parentId && isset($page['children']) && $page['children']) {
                    foreach ($page['children'] as $childKey => $childPage) {
                        $prevChildInfo = DB::table('menus')->where(['title' => $childPage['title'], 'parent_id' => $parentId])->first();
                        if ($prevChildInfo == null) {
                            $storeChildData = [
                                'title' => $childPage['title'],
                                'url' => $childPage['url'],
                                'parent_id' => $parentId,
                                'position' => $childKey
                            ];
                            DB::table('menus')->insert($storeChildData);
                        }


                    }

                }

            }
        }

        Session::flash('success','Deleted Message Successfully');
        return redirect()->back();
    }


    public function jstreeFormat()
    {
        $jstreeData = [];
        foreach (Menu::select('id', 'parent_id', 'title', 'position', 'status')->orderBy('position', 'asc')->get() as $value) {
            if ($value->parent_id == "" || $value->parent_id == null) {
                $parent = "#";
            } else {
                $parent = $value->parent_id;
            }
            if ($value->status == 'active') {
                $selected = true;
            } else {
                $selected = false;
            }
            array_push($jstreeData,
                [
                    'text' => $value->title,
                    'parent' => $parent,
                    'id' => $value->id,

                    'state' => [
                        'selected' => $selected
                    ]
                ]
            );
        }
        return $jstreeData;
    }

    public function saveTree(Request $request)
    {


        foreach ($request->id as $key => $parent_id) {
            $parentSelected = $request->selected[$key];
            if ($parentSelected == "true") {
                $parentStatus = 'active';
            } else {
                $parentStatus = 'inactive';
            }


            Menu::find($parent_id)->update(['position' => $key, 'parent_id' => 0, 'status' => $parentStatus]);

            if (isset($request->children[$parent_id])) {
                foreach ($request->children[$parent_id]['id'] as $cKey => $child) {

                    $childSelected = $request->children[$parent_id]['selected'][$cKey];
                    if ($childSelected == "true") {
                        $childStatus = 'active';
                    } else {
                        $childStatus = 'inactive';
                    }

                    Menu::find($child)->update(['position' => $key, 'parent_id' => $parent_id, 'status' => $childStatus]);
                }

            }


        }

    }

    private function navMenus()
    {
        return [
            [
                'title' => 'Home',
                'url' => url('/'),
                'children' => []
            ],
            [
                'title' => 'About Us',
                'url' =>url('about-us'),
                'children' => []
            ],
            [
                'title' => 'News & Events',
                'url' =>url('news-event'),
                'children' => []
            ],

            [
                'title' => 'Notices',
                'url' =>url('notice'),
                'children' => []
            ],

            [
                'title' => 'Contact',
                'url' => url('/contact'),
                'children' => []
            ],
            [
                'title' => 'Gallery',
                'url' => url('/gallery'),
                'children' => []
            ],
            [
                'title' => 'Admissions',
                'url' => url('admission'),
                'children' => []
            ],
            [
                'title' => 'Academics',
                'url' => url('/academic'),
                'children' => $this->getChildrenList('Academic', url('academic/'))
            ],

        ];
    }

    private function getChildrenList($page, $url)
    {
        $mappedPageMenu = [];

        $pageResult = Page::where(['status' => 'active', 'page_type' => $page])->get();

        foreach ($pageResult as $i => $data) {
            array_push($mappedPageMenu, ['title' => $data->title, 'url' => $url . '/' . str_replace(' ', '-', $data->title)]);
        }


        return $mappedPageMenu;
    }
}
