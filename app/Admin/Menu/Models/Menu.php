<?php

namespace App\Admin\Menu\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table='menus';
    protected $fillable=['id','title','featured_img','status','parent_id','url','position'];

}
