<?php

namespace App\Admin\Dashboard\Controllers;







use App\Admin\Message\Models\Message;
use App\Admin\Message\Repositories\MessageRepository;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;


class DashboardController extends Controller
{
    protected $data;
    private $customerMessage;
    use AdminTrait;
    public function __construct(MessageRepository $message)
    {
        $this->customerMessage=$message;
        $this->data['title']='Dashboard';

    }
    public function index(){
        $this->data['countCustomerMessages']= $this->customerMessage->count();
        return view('admin.dashboard')->with($this->data);
    }


}
