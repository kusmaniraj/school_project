<?php

namespace App\Admin\Setting\Controllers;



use App\Admin\Setting\Requests\SettingRequest;
use App\Admin\Setting\Repositories\SettingRepository;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;


class SettingController extends Controller
{
    protected $data;
    private $setting;
    use AdminTrait;
    public function __construct(SettingRepository $settingRepo)
    {
//        load repositories
        $this->setting=$settingRepo;
        $this->data['title']='General Setting';




    }

    public function index(){


        $this->data['setting']=$this->setting->getRow();
       return view('admin.setting.general')->with($this->data);
    }
    public function store(SettingRequest $request,$id=null){
        $formInputs=$request->all();

        if($id){
            $this->update($formInputs,$id);

        }else{
            $this->insert($formInputs);
        }
        return redirect()->back();
    }
    public function insert($formInputs){
        $result=$this->setting->create($formInputs);
        if($result){
            Session::flash('success','Created Setting Successfully');
        }else{
            Session::flash('error',' Cannot Created Setting ');
        }

    }
    public function update($formInputs, $id){
        $result=$this->setting->update($id,$formInputs);
        if($result){
            Session::flash('success','Updated Setting Successfully');
        }else{
            Session::flash('error','Cannot Update Setting ');
        }

    }

}
