<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class MailSetting extends Model
{
    protected $table='mail_settings';
    protected $fillable=['id','driver','host','port','username','password','encryption'];

}
