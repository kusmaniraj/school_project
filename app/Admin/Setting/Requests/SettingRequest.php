<?php

namespace App\Admin\Setting\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|max:100',
            'company_short_name'=>'required',
            'company_slogan' => 'max:150',
            'company_version' => 'required|regex:/^[0-9.]+$/',
            'company_start_date' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone_number' => 'required',
           'logo_img'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'company_name.required' => '*Please Input School Name.*',
            'company_name.max' => '*School Name must not be greater than 100 character.*',
            'company_slogan.max' => '*School Slogan Name must not be greater than 150 character.*',
            'email.required' => '*Please Input School Email.*',
            'phone_number.required' => '*Please Input School Phone Number.*',
            'address.required' => '*Please Input School Address.*',
            'logo_img.required' => '*Please Choose School Logo.*',
            'company_version.required' => '*Please Input School Site Version.*',
            'company_start_date.required' => '*Please Input School Start Date.*',
        ];
    }
}
