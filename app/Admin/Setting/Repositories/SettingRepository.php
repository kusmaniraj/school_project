<?php
/**
 * Created by IntelliJ IDEA.
 * User: Niraj Kusma
 * Date: 12/8/2019
 * Time: 7:05 PM
 */
namespace App\Admin\Setting\Repositories;



use App\Admin\Setting\Models\Setting;

class SettingRepository
{
    private $setting;

    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }

    public function getRow()
    {
        return $this->setting->firstOrFail();
    }

    public function getResults()
    {
        return $this->setting->all();
    }
    public function create($data){
        return $this->setting->create($data);
    }
    public function update($id,$data){
        return $this->setting->findOrFail($id)->update($data);
    }
}
