<?php

namespace App\Admin\Setting\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table='settings';
    protected $fillable=['id','company_name','company_short_name','company_slogan','logo_img','favicon_img','email','address','phone_number','fax_number','company_version','facebook_url','google_plus_url','skype_url','twitter_url','google_map_iframe','company_version','company_start_date'];
}
