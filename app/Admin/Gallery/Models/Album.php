<?php

namespace App\Admin\Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'albums';
    protected $fillable = ['id', 'albumName', 'description', 'status', 'uploadDate', 'featuredImg'];

    public function AlbumImages()
    {
        return $this->hasMany(AlbumImage::class, 'albumId', 'id');
    }
}
