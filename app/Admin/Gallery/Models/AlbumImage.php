<?php

namespace App\Admin\Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class AlbumImage extends Model
{
    protected $table = 'album_images';
    protected $fillable = ['id', 'image', 'albumId'];
    public function Album(){
        return $this->belongsTo(Album::class, 'albumId', 'id');
    }
}
