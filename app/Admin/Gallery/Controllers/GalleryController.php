<?php

namespace App\Admin\Gallery\Controllers;


use App\Admin\Gallery\Repositories\GalleryRepository;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;
use Validator;
use Illuminate\Support\Facades\Session;


class GalleryController extends Controller
{
    protected $data;
    private $gallery;
    use AdminTrait;


    public function __construct(GalleryRepository $gallery)
    {
        $this->gallery = $gallery;
        $this->data['title'] = 'Gallery Album';
    }

    public function index()
    {
        $this->data['listGalleries'] = $this->gallery->getResultsOfAlbumsWithImages();
        return view('admin.gallery.list', $this->data);
    }


    public function show()
    {
        $this->data['subTitle'] = 'Add Album Form';
        return view('admin.gallery.form', $this->data);
    }

    public function store(Request $request)
    {
        $this->albumValidation($request);//album validation
        $result = false;
        $albumName = $request->albumName;
        if ($request->hasfile('images')) {
            $storeResult = $this->gallery->createAlbum($request->except('images'));
            if ($storeResult) {
                $uploadResult = $this->uploadImages($request->file('images'), $storeResult->id);
                if ($uploadResult) {
                    $this->gallery->selectFeaturedImg($storeResult->id, $uploadResult->image);
                }
            }
            $result = true;
        }

        if ($result) {

            Session::flash('success', 'Created Gallery Album (' . $albumName . ')');
            return redirect()->route('gallery.index');
        } else {
            Session::flash('error', 'Cannot Created Gallery Album (' . $albumName . ')');
            return redirect()->back();
        }


    }


    public function edit($id)
    {
        $this->data['subTitle'] = 'Edit Album Form';
        $this->data['editGallery'] = $this->gallery->getRowOfAlbum($id);
        $this->data['countImages'] = count($this->gallery->getResultsOfAlbumImages($id));
        return view('admin.gallery.form', $this->data);

    }


    public function update(Request $request, $id)
    {
        $this->albumValidation($request);//album validation

        $albumName = $request->albumName;
        $updateResult = $this->gallery->updateAlbum($id, $request->all());

        if ($updateResult) {
            Session::flash('success', 'Updated Gallery Album (' . $albumName . ')');
        } else {
            Session::flash('error', 'Cannot Updated Gallery Album (' . $albumName . ')');
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        $galleryInfo = $this->gallery->getRowOfAlbum($id);
        $result = $this->gallery->deleteAlbum($id);
        $albumName = $galleryInfo['albumName'];
        if ($result) {
            File::deleteDirectory(public_path() . '/files/1/gallery/' . $id);
            Session::flash('success', 'Deleted Gallery Album (' . $albumName . ')');
        } else {
            Session::flash('error', 'Cannot Deleted Gallery Album (' . $albumName . ')');
        }
        return redirect()->back();
    }

    public function showAlbumImages($albumId)
    {
        $this->data['subTitle'] = 'Album Images';
        $this->data['listImages'] = $this->gallery->getResultsOfAlbumImagesWithAlbum($albumId);
        $this->data['albumId'] = $albumId;
        return view('admin.gallery.list_images', $this->data);
    }

    public function storeAlbumImages(Request $request)
    {
        $this->imageValidation($request);//image validation

        $result = false;
        if ($request->hasFile('images')) {
            $result = $this->uploadImages($request->images, $request->albumId);
        }

        if ($result) {
            Session::flash('success', 'Added Images for Gallery Album ');
        } else {
            Session::flash('error', ' Cannot Added Images for Gallery Album');
        }
        return redirect()->back();
    }

    public function uploadImages($images, $albumId)
    {

        $result = false;
        foreach ($images as $image) {
            $imageName = date('YmdHis') . $image->getClientOriginalName();
            $path = '/files/1/gallery/' . $albumId . '/';
            $image->move(public_path() . $path, $imageName);

            $result = $this->gallery->createAlbumImage(['albumId' => $albumId, 'image' => $path . $imageName]);

        }
        return $result;
    }


    public function deleteImage($imageId)
    {

        $imageInfo = $this->gallery->getRowOfImage($imageId);
        $pathImage = $imageInfo->image;
        $result = $this->gallery->deleteAlbumImage($imageId);

        if ($result) {
            @unlink(public_path() . $pathImage);
            Session::flash('success', 'Deleted Image');
        } else {
            Session::flash('error', ' Cannot Deleted Image ');
        }
        return redirect()->back();
    }

    public function changeCoverImage($albumId, $imageId)
    {

        $imageInfo = $this->gallery->getRowOfImage($imageId);

        $result = $this->gallery->selectFeaturedImg($albumId, $imageInfo->image);
        if ($result) {

            Session::flash('success', 'Change Album Covered Image');
        } else {
            Session::flash('error', ' Cannot Change Album Covered Image ');
        }
        return redirect()->back();

    }

    public function albumValidation(Request $request)
    {
        $rule = [
            'albumName' => 'required|max:100',
            'description' => 'required|max:255',
            'uploadDate' => 'required|date_format:Y-m-d',
            'images' => 'required',
            'filename.*' => 'image|mimes:jpeg,png,jpg'


        ];
        $message = [
            'albumName.required' => '*Please Input a Album Name*',
            'description.required' => '*Please Input a Description *',
            'uploadDate.required' => '*Please Input Upload Date*',
            'images.required' => '*Please Choose  Image(s)*',


        ];
        $request->validate($rule, $message);

    }

    public function imageValidation(Request $request)
    {
        $rule = [
            'albumId' => 'required',
            'images' => 'required',
            'images.*' => 'mimes:jpeg,bmp,png'];
        $message = [
            'albumId.required' => '*Album is required.*',
            'images.required' => '*Please Choose a Image(s) *',



        ];
        $request->validate($rule, $message);

    }

}
