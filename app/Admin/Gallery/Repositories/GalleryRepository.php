<?php
namespace App\Admin\Gallery\Repositories;


use App\Admin\Gallery\Models\Album;
use App\Admin\Gallery\Models\AlbumImage;

class GalleryRepository
{
    private $album;
    private $albumImage;

    public function __construct(Album $album,AlbumImage  $albumImage)
    {
        $this->album=$album;
        $this->albumImage=$albumImage;
    }
   public function getResultsOfAlbum(){
      return Album::all();
   }
    public function getResultsOfAlbumsWithImages(){
        return Album::with(['AlbumImages'])->get();
    }
    public function getRowOfAlbum($id){
        return Album::findorFail($id);
    }
    public function createAlbum(array $data){
        return Album::create($data);
    }
    public function updateAlbum($id,array $data){
        return Album::findOrFail($id)->update($data);
    }
    public function deleteAlbum($id){
        return Album::findOrFail($id)->delete();
    }
    public function selectFeaturedImg($albumId, $image)
    {
        return Album::findOrFail($albumId)->update(['featuredImg' => $image]);
    }
    public function getResultsOfAlbumImages($albumId){
        return AlbumImage::where(['albumId' => $albumId])->get();
    }
    public function getResultsOfAlbumImagesWithAlbum($albumId){
        return AlbumImage::with('Album')->where(['albumId' => $albumId])->get();
    }
    public function getRowOfImage($id){
        return AlbumImage::findorFail($id);
    }
    public function createAlbumImage(array $data){
        return AlbumImage::create($data);
    }
    public function updateAlbumImage($id,array $data){
        return AlbumImage::findOrFail($id)->update($data);
    }
    public function deleteAlbumImage($id){
        return AlbumImage::findOrFail($id)->delete();
    }

}
