<?php

namespace App\Admin\Slider\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table='sliders';
    protected $fillable=['id','title','featured_img','status','description','page_type','url'];
}
