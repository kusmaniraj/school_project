<?php
/**
 * Created by IntelliJ IDEA.
 * User: Niraj Kusma
 * Date: 12/8/2019
 * Time: 7:05 PM
 */
namespace App\Admin\Slider\Repositories;





use App\Admin\Slider\Models\Slider;

class SliderRepository
{
    private $slider;

    public function __construct(Slider $slider)
    {
        $this->slider = $slider;
    }

    public function getRow($id)
    {
        return $this->slider->findOrFail($id);
    }



    public function getResults()
    {
        return $this->slider->orderBy('id','desc')->all();
    }

    public function create(array $data)
    {
        return $this->slider->create($data);
    }

    public function update($id, array $data)
    {
        return $this->slider->findOrFail($id)->update($data);
    }
    public function delete($id)
    {
        return $this->slider->findOrFail($id)->delete();
    }
}
