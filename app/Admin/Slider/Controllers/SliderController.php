<?php

namespace App\Admin\Slider\Controllers;



use App\Admin\Slider\Repositories\SliderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;
use Infrastructure\Http\Traits\DataArrayTrait;


class SliderController extends Controller
{
    protected $data;
    private $slider;
    use AdminTrait;
    use DataArrayTrait;


    public function __construct(SliderRepository $sliderRepository)
    {
        $this->slider = $sliderRepository;
        $this->data['title'] = 'Sliders';
        $this->data['pageTypes'] = $this->pageTypeArray();


    }

    /**
     * List of Sliders
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->data['listSliders'] = $this->slider->getResults();
        return view('admin.slider.list', $this->data);
    }

    /**
     * Show Slider Form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('admin.slider.form', $this->data);
    }

    /**
     * Create Slider Form Data
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validation($request);//validation
        $resp = $this->slider->create($request->all());
        $alertMessage = ' a slider (' . $request->title . ')';
        if ($resp) {
            Session::flash('success', 'Created' . $alertMessage);
            return redirect()->route('slider.edit', $resp->id);
        } else {
            Session::flash('success', ' Cannot Create' . $alertMessage);
            return redirect()->back();
        }

    }

    /**
     * Edit Slider Form
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $sliderInfo = $this->slider->getRow($id);
        $this->data['editSlider'] = $sliderInfo;
        return view('admin.slider.form', $this->data);

    }

    /**
     * Update Slider Form Data
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validation($request);//validation
        $resp = $this->slider->update($id, $request->all());
        $alertMessage = ' a slider(' . $request->title . ')';
        if ($resp) {
            Session::flash('success', 'Updated' . $alertMessage);
        } else {
            Session::flash('success', ' Could not Update' . $alertMessage);
        }
        return redirect()->route('slider.edit', $id);

    }

    /**
     * Delete Slider
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $sliderInfo = $this->slider->getRow($id);
        $resp = $this->slider->delete($id);
        $alertMessage = ' a ' . $sliderInfo->slider_name . ' (' . $sliderInfo->title . ')';
        if ($resp) {
            Session::flash('success', 'Deleted' . $alertMessage);

        } else {
            Session::flash('success', ' Could  not Delete' . $alertMessage);

        }
        return redirect()->back();
    }

    /**
     * Validation of Slider Required Field
     * @param Request $request
     */
    private function validation(Request $request)
    {
        $rules = ['title' => 'required|max:100',
            'description' => 'required|max:255',
            'featured_img' => 'required',
            'page_type' => 'required',
            'url' => 'required'];
        $messages = [
            'title.required' => '*Title is required for Slider.*',
            'title.max' => '*Title must not be greater than 100 .*',
            'description.required' => '*Description is required for Slider.*',
            'description.max' => '*Description must not be greater than 100.*',
            'featured_img.required' => '*Image is required for Slider.*',
            'page_type.required' => '*Please select Page for url.*',
            'url.required' => '*Please select  any List of Page for url.*',
        ];
        $request->validate($rules, $messages);
    }


}
