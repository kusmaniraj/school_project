<?php

namespace App\Admin\File\Controllers;


use Illuminate\Http\Request;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;


class FileManager extends Controller
{
    protected $data;
    use AdminTrait;
    public function __construct()
    {
        $this->data['title']='File Manager';
    }

    public function index(){

        return view('admin.file_manager',$this->data);
    }
}
