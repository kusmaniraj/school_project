<?php

namespace App\Admin\Message\Controllers;




use App\Admin\Message\Models\Message;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Session;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;


class MessageController extends Controller
{
    protected  $data;
use AdminTrait;
    public function __construct()
    {
        $this->data['title']='Customer Messages';





    }

    public function index(){


        $this->data['messages']=Message::all();
        return view('admin.customer_message.list')->with($this->data);
    }


    public function destroy($id){
        $result=Message::find($id)->delete();
        if($result){
            Session::flash('success','Deleted Message Successfully');
        }else{
            Session::flash('error','Cannot Delete  Message');
        }
        return redirect()->back();

    }
}
