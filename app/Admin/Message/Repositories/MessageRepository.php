<?php
namespace App\Admin\Message\Repositories;
use App\Admin\Message\Models\Message;

class MessageRepository
{
    private $customerMessage;

    public function __construct(Message $message)
    {
        $this->customerMessage=$message;
    }
    public function getResults($columns=null,$whereArray=null){
        if($columns==null){
            $columns='*';
        }
        $query=$this->customerMessage;
        if($whereArray){
            $query->where($whereArray);
        }
        return $query->get($columns);
    }

    public function count(){
        return $this->customerMessage->count();
    }
}
