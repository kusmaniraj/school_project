<?php

namespace App\Admin\Message\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table='messages';
    protected $fillable=['id','name','company_name','email','phone_number','message','status'];
}
