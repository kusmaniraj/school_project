<?php

namespace App\Admin\Student\Controllers;


use App\Admin\Page\Models\Page;
use App\Admin\Student\Requests\StudentReviewRequest;
use App\Admin\Student\Models\StudentReview;
use Illuminate\Support\Facades\Session;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;
use Infrastructure\Http\Traits\DataArrayTrait;

class StudentReviewController extends Controller
{
    protected $data;
    use AdminTrait;
    use DataArrayTrait;


    public function __construct()
    {
        $this->data['title'] = 'Student Reviews';
        $this->data['academicsList'] = $this->getResultOfAcademic();


    }

    private function getResultOfAcademic()
    {
        return Page::where(['page_type'=>'academic','status'=>'active'])->get();

    }

    public function index()
    {
        $this->data['listStudentReviews'] = $this->getStudentReviews();
        return view('admin.student_review.list', $this->data);
    }

    public function show()
    {
        return view('admin.student_review.form', $this->data);
    }


    public function store(StudentReviewRequest $request)
    {

        $result = StudentReview::create($request->all());
        $alertMessage = ' a Student StudentReview' . ' (' . $request->title . ')';
        if ($result) {
            Session::flash('success', 'Created' . $alertMessage);
            return redirect()->route('studentReview.edit', $result->id);
        } else {
            Session::flash('success', ' Could  not Created' . $alertMessage);

        }
        return redirect()->back();
    }

    public function edit($id)
    {
        $this->data['editStudentReview'] = $this->getStudentReview($id);
        return view('admin.student_review.form', $this->data);
    }

    public function update(StudentReviewRequest $request, $id)
    {

        $result = StudentReview::find($id)->update($request->all());
        $alertMessage = ' a Student StudentReview' . ' (' . $request->title . ')';
        if ($result) {
            Session::flash('success', 'Updated' . $alertMessage);
        } else {
            Session::flash('success', ' Could  not Updated' . $alertMessage);
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        $result = StudentReview::findOrFail($id)->delete();
        $studentReviewInfo = $this->getStudentReview($id);
        $alertMessage = ' a Student StudentReview' . ' (' . $studentReviewInfo->title . ')';
        if ($result) {
            Session::flash('success', 'Deleted' . $alertMessage);
        } else {
            Session::flash('success', ' Could  not Deleted' . $alertMessage);
        }
        return redirect()->back();
    }

    private function getStudentReviews()
    {
        return StudentReview::all();
    }

    private function getStudentReview($id)
    {
        return StudentReview::findOrFail($id);
    }
}
