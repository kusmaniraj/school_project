<?php

namespace App\Admin\Student\Models;

use Illuminate\Database\Eloquent\Model;

class StudentReview extends Model
{
    protected $table='student_reviews';
    protected $fillable=['id','title','profileImg','name','status','academic','description','grade'];
}
