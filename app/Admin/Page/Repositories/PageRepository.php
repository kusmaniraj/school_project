<?php
/**
 * Created by IntelliJ IDEA.
 * User: Niraj Kusma
 * Date: 12/8/2019
 * Time: 7:05 PM
 */
namespace App\Admin\Page\Repositories;


use App\Admin\Page\Models\Page;


class PageRepository
{
    private $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function getRowOfPageById($id)
    {
        return $this->page->findOrFail($id);
    }

    public function getRowOfPageByType($type)
    {
        return $this->page->where(['page_type' => $type])->first();
    }

    public function getPageResultsByType($type)
    {
        return $this->page->where(['page_type' => $type])->orderBy('id', 'asc')->get();
    }

    public function create(array $data)
    {
        return $this->page->create($data);
    }

    public function update($id, array $data)
    {
        return $this->page->findOrFail($id)->update($data);
    }
    public function delete($id)
    {
        return $this->page->findOrFail($id)->delete();
    }
}
