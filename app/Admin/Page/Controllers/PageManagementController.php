<?php
namespace App\Admin\Page\Controllers;



use App\Admin\Page\Models\Page;
use App\Admin\Page\Repositories\PageRepository;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Session;
use Infrastructure\Http\Controllers\Controller;
use Infrastructure\Http\Traits\AdminTrait;
use Infrastructure\Http\Traits\DataArrayTrait;
use Infrastructure\Http\Traits\HelperTrait;
use Validator;

class PageManagementController extends Controller
{
    protected $data;
    private $page;
    use AdminTrait;
    use DataArrayTrait;
    use HelperTrait;




    public function __construct(PageRepository $page)
    {
        $this->data['parentTitle'] = 'Page';
        $this->page=$page;
    }

    public function index($pageType)
    {
//        check the page
        if ($this->checkPage($pageType) == false) {
            abort(404);
        }


        $this->data['pageResults'] = $this->page->getPageResultsByType($pageType);
        $this->data['title'] =$this->trimTitle($pageType);
        $this->data['pageType'] = $pageType;
        return view('admin.page.list', $this->data);
    }

    public function show($pageType)
    {
//        check the page
        if ($this->checkPage($pageType) == false) {
            abort(404);
        }
        $this->data['pageType'] = $pageType;
        $this->data['title'] =$this->trimTitle($pageType);
        return view('admin.page.form', $this->data);
    }


    public function store(Request $request)
    {


//    validation
        $this->validation($request);
//        end of validation
        $resp = $this->page->create($request->all());
        $alertMessage = ' a ' . $request->page_type . ' (' . $request->title . ')';
        if ($resp) {
            Session::flash('success', 'Created' . $alertMessage);
            return redirect()->route('page.edit', $resp->id);
        } else {
            Session::flash('success', ' Cannot Create' . $alertMessage);
            return redirect()->back();
        }

    }

    public function edit($id)
    {
        $pageInfo = $this->page->getRowOfPageById($id);
        $this->data['editPage'] = $pageInfo;
        $this->data['pageType'] = $pageInfo->page_type;
        $this->data['title'] = ucfirst($this->data['pageType']);

        return view('admin.page.form', $this->data);

    }

    public function update(Request $request, $id)
    {
//        validation
        $this->validation($request);
//        end of validation
        $resp =$this->page->update($id,$request->all());
        $alertMessage = ' a ' . $request->page_type . ' (' . $request->title . ')';
        if ($resp) {
            Session::flash('success', 'Updated' . $alertMessage);

        } else {
            Session::flash('success', ' Could not Update' . $alertMessage);
        }

        return redirect()->back();


    }

    public function destroy($id)
    {
        $pageInfo=$this->page->getRowOfPageById($id);
        $resp = $this->page->delete($id);
        $alertMessage = ' a ' . $pageInfo->page_type . ' (' . $pageInfo->title . ')';
        if ($resp) {
            Session::flash('success', 'Deleted' . $alertMessage);

        } else {
            Session::flash('success', ' Could  not Delete' . $alertMessage);

        }
        return redirect()->back();
    }





    private function checkPage($pageName)
    {
        $status = false;
        foreach ($this->pageTypeArray() as $page) {
            if ($page == $pageName) {
                $status = true;
                break;
            }
        }
        return $status;


    }


    private function validation(Request $request)
    {


        $rules=[];
        $messages=[];
        switch($request->page_type){
            case $request->page_type=='Event':
                $rules=[
                    'title'=>'required|alpha',
                    'description' => 'required',
                    'start_time'=>'required|before:end_time',
                    'end_time'=>'required',
                    'date'=>'required|date_format:Y-m-d',
                    'address'=>'required',
                ];
                $messages=['start_time.before'=>'Start Time must be before time of end time.'];

                break;
            case $request->page_type=='News' || $request->page_type=='Notice':
                $rules=[
                    'title'=>'required',
                    'description' => 'required',
                    'date'=>'required|date_format:Y-m-d',

                ];
                break;

            default:
                $rules=[
                    'title'=>'required',
                    'description' => 'required',


                ];


        }

        $request->validate($rules,$messages);
    }
}
