<?php

namespace App\Admin\Page\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Page extends Model
{
    protected $table='pages';
    protected $fillable=['id','page_type','title','featured_img','status','description','date','start_time','end_time','address'];



}
