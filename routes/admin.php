<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin','namespace'=>'Admin'], function () {
    Route::group(['namespace'=>'Auth\Controllers'], function () {
        // Authentication Routes...
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');
        Route::post('logout', 'LoginController@logout')->name('logout');

// Registration Routes...
        Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'RegisterController@register');

// Password Reset Routes...
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/update', 'ResetPasswordController@reset')->name('password.update');
        ;
    });



});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
//    admin profile
    Route::group(['prefix' => 'profile','namespace' => 'Auth\Controllers'], function () {
        Route::get('/', 'ProfileController@index')->name('profile.index');
        Route::post('/update/{id?}', 'ProfileController@update')->name('profile.update');
    });
//    dashboard
    Route::group(['namespace' => 'Dashboard\Controllers'], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard.index');

    });


    //   Menu
    Route::group(['prefix'=>'menu','namespace' => 'Menu\Controllers'],function(){
        Route::get('/','MenuController@index')->name('menu.index');
        Route::get('/getMenus','MenuController@getMenus')->name('menu.getMenus');

        Route::post('/store','MenuController@store')->name('menu.store');
        Route::post('/saveTree','MenuController@saveTree')->name('menu.saveTree');
        Route::get('/{id}/edit','MenuController@edit')->name('menu.edit');
        Route::post('/update/{id}','MenuController@update')->name('menu.update');
        Route::get('/refresh/','MenuController@refreshMenuPages')->name('menu.refresh');


    });




//    // setting
    Route::group(['prefix' => 'setting','namespace' => 'Setting\Controllers'], function () {
        Route::get('/', 'SettingController@index')->name('setting');
        Route::post('/store/{id?}', 'SettingController@store')->name('setting.store');

    });

//  Slider
    Route::group(['prefix' => 'slider','namespace' => 'Slider\Controllers'], function () {
        Route::get('/', 'SliderController@index')->name('slider.index');
        Route::get('/show/', 'SliderController@show')->name('slider.show');
        Route::post('/store', 'SliderController@store')->name('slider.store');
        Route::get('/{id}/edit', 'SliderController@edit')->name('slider.edit');
        Route::post('/update/{id}', 'SliderController@update')->name('slider.update');
        Route::get('/delete/{id}', 'SliderController@destroy')->name('slider.delete');


    });


//    page Management
    Route::group(['prefix' => 'page','namespace' => 'Page\Controllers'], function () {


        Route::get('/{type}', 'PageManagementController@index')->name('page.index');
        Route::get('/show/{type}', 'PageManagementController@show')->name('page.show');
        Route::post('/store', 'PageManagementController@store')->name('page.store');
        Route::get('/{id}/edit', 'PageManagementController@edit')->name('page.edit');
        Route::post('/update/{id}', 'PageManagementController@update')->name('page.update');
        Route::get('/delete/{id}', 'PageManagementController@destroy')->name('page.delete');

    });

    //   student_reviews
    Route::group(['prefix' => 'student_review','namespace' => 'Student\Controllers'], function () {
        Route::get('/', 'StudentReviewController@index')->name('studentReview.index');
        Route::get('/show', 'StudentReviewController@show')->name('studentReview.show');
        Route::post('/store', 'StudentReviewController@store')->name('studentReview.store');
        Route::get('/{id}/edit', 'StudentReviewController@edit')->name('studentReview.edit');
        Route::post('/update/{id}', 'StudentReviewController@update')->name('studentReview.update');
        Route::get('/delete/{id}', 'StudentReviewController@destroy')->name('studentReview.delete');

    });


    //   leaderships
    Route::group(['prefix' => 'leadership','namespace' => 'Leadership\Controllers'], function () {
        Route::get('/', 'LeadershipController@index')->name('leadership.index');
        Route::get('/show', 'LeadershipController@show')->name('leadership.show');
        Route::post('/store', 'LeadershipController@store')->name('leadership.store');
        Route::get('/{id}/edit', 'LeadershipController@edit')->name('leadership.edit');
        Route::post('/update/{id}', 'LeadershipController@update')->name('leadership.update');
        Route::get('/delete/{id}', 'LeadershipController@destroy')->name('leadership.delete');

        Route::group(['prefix' => 'social_link'], function () {

            Route::get('/getSocialLinks/{leadership_id}', 'SocialLinkController@getSocialLinks')->name('social_link.getSocialLinks');
            Route::post('/store', 'SocialLinkController@store')->name('social_link.store');
            Route::get('/{id}/edit', 'SocialLinkController@edit')->name('social_link.edit');
            Route::post('/update/{id}', 'SocialLinkController@update')->name('social_link.update');
            Route::get('/delete/{id}', 'SocialLinkController@destroy')->name('social_link.delete');

        });

    });
    //   gallery
    Route::group(['prefix' => 'gallery','namespace' => 'Gallery\Controllers'], function () {
        Route::get('/', 'GalleryController@index')->name('gallery.index');
        Route::get('/show', 'GalleryController@show')->name('gallery.show');
        Route::post('/store', 'GalleryController@store')->name('gallery.store');
        Route::get('/{id}/edit', 'GalleryController@edit')->name('gallery.edit');
        Route::post('/update/{id}', 'GalleryController@update')->name('gallery.update');
        Route::get('/delete/{id}', 'GalleryController@destroy')->name('gallery.delete');

        Route::get('/images/show/{albumId}', 'GalleryController@showAlbumImages')->name('image.showAlbumImages');
        Route::post('/images/store', 'GalleryController@storeAlbumImages')->name('image.storeAlbumImages');
        Route::get('/image/delete/{id}', 'GalleryController@deleteImage')->name('image.deleteImage');
        Route::get('/image/cover/{albumId}/{id}', 'GalleryController@changeCoverImage')->name('image.changeCoverImage');



    });


////customer Message
    Route::group(['prefix' => 'message','namespace'=>'Message\Controllers'], function () {
        Route::get('/', 'MessageController@index')->name('message.index');

        Route::get('/delete/{id}', 'MessageController@destroy')->name('message.delete');


    });

//    file manager
    Route::get('/file_manager', 'File\Controllers\FileManager@index')->name('admin.file_manager');
//
////    page Management
//    Route::group(['prefix' => 'contact'], function () {
//        Route::get('/{type}', 'ContactController@index')->name('contact.index');
//        Route::get('/getContacts/{pageName}', 'ContactController@getContacts')->name('contact.getContacts');
//        Route::post('/store', 'ContactController@store')->name('contact.store');
//        Route::get('/{id}/edit', 'ContactController@edit')->name('contact.edit');
//        Route::post('/update/{id}', 'ContactController@update')->name('contact.update');
//        Route::get('/delete/{id}', 'ContactController@destroy')->name('contact.delete');
//
//
//    });


});

