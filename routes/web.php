<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//Web route
Route::group(['namespace' => 'Web\Controllers'], function () {
    Route::get('/', 'WebController@home')->name('web.home');
    Route::get('/news-event', 'WebController@newsAndEventsPage')->name('web.newAndEvent');
    Route::get('/about-us', 'WebController@aboutUsPage')->name('web.aboutUs');
    Route::get('/contact', 'WebController@contactPage')->name('web.contact');
    Route::get('/gallery', 'WebController@albumPage')->name('web.albums');
    Route::get('/gallery/images/{albumTitle}', 'WebController@albumImagesPage')->name('web.albumImages');
    Route::post('/send/message', 'WebController@sendCustomerMessage')->name('web.sendMessage');
    Route::get('/admission', 'WebController@admissionPage')->name('web.admission');
    Route::get('/academic', 'WebController@academicPage')->name('web.academic');
    Route::get('/academic/{title}', 'WebController@academicDetailPage')->name('web.academic.detail');
    Route::get('/notice', 'WebController@noticePage')->name('web.notice');
    Route::get('/notice/{title}', 'WebController@noticeDetailPage')->name('web.notice.detail');
    Route::get('/getRowPageInfo/{pageId}', 'WebController@getRowPageInfo');

});
